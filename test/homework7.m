%% Homework 7 Jonny Wong
clc; close all; clear;

%% Question 1
% Part B
w = [0;1;0];
R = [0.5 -0.866 0; 0.866 0.5 0; 0 0 1];

Rdot = cpMap(w) * R

% Part C
w = [1;0;0];
R = [0.5 -0.866 0; 0.866 0.5 0; 0 0 1];

Rdot = cpMap(w) * R

% Part D
w = [0.9134;0.6324;0.09754];
R = [-0.6217 -0.02073 -0.783; -0.1385 -0.981 0.1359; -0.7709 0.193 0.607];

Rdot = cpMap(w) * R

%% Question 3

T1 = dhTransform(0, 1, pi/2, pi/3);
T2 = dhTransform(0.5, 0, 0, pi/6);
T3 = dhTransform(0.3, 0, 0, pi/6);

q = [pi/3; pi/6; pi/6];
q_dot = [0.5; -1.0; 1.5];
link_a = [0; 0.5; 0.3];
link_alpha = [pi/2; 0; 0];
link_d = [1; 0; 0];
T_init = eye(4);
alpha = [0; 0; 0];
v_init = [0; 0; 0];
a_init = [0; 0; 0];
w_init = [0;0;0];


    T = T_init;
    R = T(1:3, 1:3);
    d = T(1:3, 4);
    w = w_init + q_dot(1)*R(1:3,3)
    v = v_init + cross(w, (R*d))
    a = a_init + cross(w, cross(w, R*d)) + cross(alpha, R*d)
    
    % Assign current to init
    v_init = v;
    a_init = a;
    w_init = w;


for i = 1:3
    T = T_init * dhTransform(link_a(i), link_d(i), link_alpha(i), q(i));
    R = T(1:3, 1:3);
    d = T_init(1:3, 4);
    w = w_init + q_dot(i)*R(1:3,3)
    v = v_init + cross(w, (R*d))
    a = a_init + cross(w, cross(w, R*d)) + cross(alpha, R*d)
    
    % Assign current to init
    v_init = v;
    a_init = a;
    w_init = w;
    T_init = dhTransform(link_a(i), link_d(i), link_alpha(i), q(i));
end

