%% Homework 6 Jonny Wong
clc; close all; clear;

%% Question 1 Part A
P0 = [1.00, 0.25, 0.25];
Pf = [0.5, -0.5, 0];
V0 = [0, 0, 0];
Vf = [0, 0, 0];

a = cubic_coeff(P0, Pf, V0, Vf, 2); % Returns 

%% Question 1 Part B
t0 = 0; %initial time
inc = .1; %increment between times
time = [t0:inc:2]; %times

[P_f] = cubic_interp(a, time, t0);

%% Question 1 Part C

% Calculate theta values
T_init = [1 0 0 1.00; 0 0 -1 0.25; 0 1 0 0.25; 0 0 0 1];
T_final = [0 -1 0 0.5; 1 0 0 -0.5; 0 0 1 0; 0 0 0 1];

th1_vals = zeros(100,1);

[th1_init, th2_init, th3_init, th4_init, th5_init, th6_init, d3_init] = calc_thetas(1, 0.5, T_init);
[th1_f, th2_f, th3_f, th4_f, th5_f, th6_f, d3_f] = calc_thetas(1, 0.5, T_final);

xp = zeros(length(time), 1);
yp = zeros(length(time), 1);
zp = zeros(length(time), 1);

Th0 = [th1_init(1,1), th2_init(1,1), th3_init(1,1), th4_init(1,1), th5_init(1,1), th6_init(1,1)];
Thf = [th1_f(1,1), th2_f(1,1), th3_f(1,1), th4_f(1,1), th5_f(1,1), th6_f(1,1)];

V0 = zeros(length(Th0), 1);
Vf = zeros(length(Th0), 1);

a = cubic_coeff(Th0, Thf, V0', Vf', 2); % Returns 

[Th_f] = cubic_interp(a, time, t0);

for i = 1:length(time)
    T_step = dhTransform(1, 0, 0, Th_f(i,1))*dhTransform(0.5, 0, 0, Th_f(i,2))*...
        dhTransform(0, P_f(i,3), 0, 0)*dhTransform(0, 0, pi/2, Th_f(i,4))*...
        dhTransform(0, 0, -pi/2, Th_f(i,5))*dhTransform(0, 0, 0, Th_f(i,6));
    
    drawArm(Th_f(i,1), Th_f(i,2), P_f(i,3), Th_f(i,4), Th_f(i,5), Th_f(i,6), 1, T_step);
    xp(i) = T_step(1,4);
    yp(i) = T_step(2,4);
    zp(i) = T_step(3,4);
    F(i) = getframe;
end

writerObj = VideoWriter('interp_position.avi');
open(writerObj);
writeVideo(writerObj, F)
close(writerObj);

figure();
plot3(xp,yp,zp);
xlabel('X');
ylabel('Y');
zlabel('Z');
title("Theta Space");


%% Angle Axis space
[ki, aaTheta_init] = rot2AngleAxis(T_init(1:3,1:3));
[kf, aaTheta_final] = rot2AngleAxis(T_final(1:3,1:3));

a = cubic_coeff(aaTheta_init, aaTheta_final, 0, 0, 2);

[P_f] = cubic_interp(a, time, t0);

xa = zeros(length(time), 1);
ya = zeros(length(time), 1);
za = zeros(length(time), 1);

for i = 1:length(time)

    T_step = dhTransform(1, 0, 0, Th_f(i,1))*dhTransform(0.5, 0, 0, Th_f(i,2))*...
        dhTransform(0, P_f(i), 0, 0)*dhTransform(0, 0, pi/2, Th_f(i,4))*...
        dhTransform(0, 0, -pi/2, Th_f(i,5))*dhTransform(0, 0, 0, Th_f(i,6));
    
    xa(i) = T_step(1,4);
    ya(i) = T_step(2,4);
    za(i) = T_step(3,4);
end

figure()
plot3(xa, ya, za, 'r');
xlabel('X');
ylabel('Y');
zlabel('Z');
title("Angle-Axis Space");

%% Twist space

P0 = transform2Twist(T_init);
Pf = transform2Twist(T_final);
V0 = zeros(length(P0),1);
Vf = zeros(length(Pf),1);

a = cubic_coeff(P0', Pf', V0', Vf', 2);

[P_f] = cubic_interp(a, time, t0);

xt = zeros(length(time), 1);
yt = zeros(length(time), 1);
zt = zeros(length(time), 1);

for i = 1:length(time)

    T_step = twist2Transform(P_f(i,1:6)');
    
    xt(i) = T_step(1,4);
    yt(i) = T_step(2,4);
    zt(i) = T_step(3,4);
end

figure();
plot3(xt, yt, zt, 'r');
xlabel('X');
ylabel('Y');
zlabel('Z');
title("Twist Space");

%% Quaternion space

P0 = rot2Quat(T_init(1:3,1:3));
Pf = rot2Quat(T_final(1:3,1:3));
V0 = zeros(length(P0),1);
Vf = zeros(length(Pf),1);

a = cubic_coeff(P0', Pf', V0', Vf', 2);

[P_f] = cubic_interp(a, time, t0);

xt = zeros(length(time), 1);
yt = zeros(length(time), 1);
zt = zeros(length(time), 1);

for i = 1:length(time)
     T_step = dhTransform(1, 0, 0, Th_f(i,1))*dhTransform(0.5, 0, 0, Th_f(i,2))*...
        dhTransform(0, P_f(i,3), 0, 0)*dhTransform(0, 0, pi/2, Th_f(i,4))*...
        dhTransform(0, 0, -pi/2, Th_f(i,5))*dhTransform(0, 0, 0, Th_f(i,6));
    
    xt(i) = T_step(1,4);
    yt(i) = T_step(2,4);
    zt(i) = T_step(3,4);
end

figure()
plot3(xt, yt, zt, 'r');
xlabel('X');
ylabel('Y');
zlabel('Z');
title("Quaternion Space");


%% CUBIC_INTERP 
%   Input:
%       a - set of constant coefficients
%       t - time matrix
%       t0 - initial time (0)
%   Output:
%       Pf - Cubic interpolated matrix
%   Dependent Methods: None
%   Description:
%       Function that calculates cubic interpolation of a matrix
%
% Jonny Wong
% CWID: 10884664
% MEGN544
% Oct 5, 2021
%
function[Pf] = cubic_interp(a, t, t0)

    Pf = a(1,:)+t'.*(a(2,:) + t'.*(a(3,:) + t'.*a(4,:)));

end


%% CUBIC_COEFF
%   Input:
%       P0 - Initial position
%       Pf - Final Position
%       V0 - Initial Velocity
%       Vf - Final Velocity
%       delta_t - Delta time
%   Output:
%       a - Cubic interpolated matrix
%   Dependent Methods: None
%   Description:
%       Function that calculates cubic coefficients of a matrix
%
% Jonny Wong
% CWID: 10884664
% MEGN544
% Oct 5, 2021
%
function[a] = cubic_coeff(P0, Pf, V0, Vf, delta_t)

    A = [1, 0, 0, 0; 1, delta_t, delta_t^2, delta_t^3; 0 1 0 0; 0 1 (2*delta_t) (3*delta_t^2)];

    posvel = [P0; Pf; V0; Vf];

    a = A\posvel;

end

%% CALC_THETAS
%   Input:
%       a1 - DH Table a1
%       a2 - DH Table a2
%       T - Transformation Matrix
%   Output:
%       th1 - Theta 1
%       Th2 - Theta 2
%       Th3 - Theta 3
%       Th4 - Theta 4
%       Th5 - Theta 5
%       Th6 - Theta 6
%   Dependent Methods: None
%   Description:
%       Function that calculates thetas
%
% Jonny Wong
% CWID: 10884664
% MEGN544
% Oct 5, 2021
%
function[th1, th2, th3, th4, th5, th6, d3] = calc_thetas(a1, a2, T)
    th1 = zeros(2,1);
    th2 = zeros(2,1);
    th5 = zeros(2,1);
    th4 = zeros(4,1);
    th6 = zeros(2,1);
    
    % Theta 2
    th2 = [1;-1]*2*atan(sqrt( ((a1 + a2)^2 - (T(1,4)^2 + T(2,4)^2)) /... 
        ((T(1,4)^2 + T(2,4)^2) - (a1 - a2)^2)));
    
    % Theta 1
    th1(1,1) = atan2(T(2,4), T(1,4)) - atan2(a2*sin(th2(1,1)), a1 + a2*cos(th2(1,1)));
    th1(2,1) = atan2(T(2,4), T(1,4)) - atan2(a2*sin(th2(2,1)), a1 + a2*cos(th2(2,1)));
    
    % Theta 5
    sqrt_val = sqrt(T(1,3)^2 + T(2,3)^2);
    th5(1,1) = atan2(sqrt_val, T(3,3));
    th5(2,1) = atan2(-sqrt_val, T(3,3));
    
    if th5(1,1) == 0
        th5(1,1) = 0.001;
        th5(2,1) = -0.001;
   end
    
    % Theta 4
    th4(1,1) = atan2( -(T(2,3)/sin(th5(1,1))), -(T(1,3)/sin(th5(1,1)))) - th1(1,1) - th2(1,1);
    th4(2,1) = atan2( -(T(2,3)/sin(th5(2,1))), -(T(1,3)/sin(th5(2,1)))) - th1(1,1) - th2(1,1);
    th4(3,1) = atan2( -(T(2,3)/sin(th5(1,1))), -(T(1,3)/sin(th5(1,1)))) - th1(2,1) - th2(2,1);
    th4(4,1) = atan2( -(T(2,3)/sin(th5(2,1))), -(T(1,3)/sin(th5(2,1)))) - th1(2,1) - th2(2,1);
    
    % Theta 6
    th6(1,1) = atan2( -(T(3,2)/sin(th5(1,1))), (T(3,1)/sin(th5(1,1))));
    th6(2,1) = atan2( -(T(3,2)/sin(th5(2,1))), (T(3,1)/sin(th5(2,1))));
    
    th3 = 0;
    
    d3 = T(3,4);
end







