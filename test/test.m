A = [0 1; 1 1];
B = [0;1];
C = [1 0];
D = 0;

Tr = .75;
Ts = 1.5;
os = 0.025; 

% Desired max os is 5% plan on 2% for fs
zeta = sqrt(log(os)^2/(pi^2+log(os)^2))
sigma = max(4.6/(Ts*.95),(1.53*zeta + 2.31*zeta^3)/(Tr*.95))

poles = -sigma*([1;5] + [sqrt(1-zeta^2)/zeta*1i;-sqrt(1-zeta^2)/zeta *1i])

%% Continuous Time Design
%zeta = sqrt(log(0.03)^2/(pi^2+log(0.03)^2))
%wn = (1.53+2.31*zeta^2)/0.5
%cl_poles = [-5*wn*zeta;-wn*(zeta+[1]*sqrt(1-zeta^2)*1j)] 


K = place(A,B,poles)
 % Closed Loop System Model
A_cl = A-B*K;
B_cl = B*(K-(B'*B)^-1*B'*A)*[1;0];
cl_sys = ss(A_cl,B_cl,C,D);
cl_step_info = stepinfo(cl_sys)
figure(1)
subplot(2,1,1)
step(cl_sys)
subplot(2,1,2)
margin(cl_sys)