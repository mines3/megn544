%% homework4 Jonny
%% Problem 4
%% Part A
close all


H_0 = dhTransform(1, 1, pi/2, 0);
H_1 = dhTransform(.5, 1, -pi/2, 0);
H_2 = dhTransform(0, 0, 0, 0);
H_3 = dhTransform(1, 0, pi/2, 0);
H_4 = dhTransform(0, 0, -pi/2, 0);
H_5 = dhTransform(0, 0, pi/2, 0);

H_6 = H_0*H_1*H_2*H_3*H_4*H_5

%% Part B

H_0 = dhTransform(1, 1, pi/2, pi/6);
H_1 = dhTransform(.5, 1, -pi/2, pi/6);
H_2 = dhTransform(0, 0, 0, pi/6);
H_3 = dhTransform(1, 0, pi/2, pi/6);
H_4 = dhTransform(0, 0, -pi/2, pi/6);
H_5 = dhTransform(0, 0, pi/2, pi/6);

H_6 = H_0*H_1*H_2*H_3*H_4*H_5

%% Problem 5
%% Part A

H_0 = dhTransform(1, 1, pi/2, 0);
H_1 = dhTransform(1, 0, pi/2, pi/2);
H_2 = dhTransform(1, 0, 0, 0);

H_6 = H_0*H_1*H_2

%% Part B

H_0 = dhTransform(1, 0, pi/2, 0);
H_1 = dhTransform(1, 1, pi/2, pi/2);
H_2 = dhTransform(1, 0, 0, 0);

H_6 = H_0*H_1*H_2

%% Part C

H_0 = dhTransform(1, 0, pi/2, 0);
H_1 = dhTransform(1, 0, pi/2, pi/2);
H_2 = dhTransform(1, 1, 0, 0);

H_6 = H_0*H_1*H_2
