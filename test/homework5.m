%% homework5 Jonny
%% Problem 1d
close all

a = 0;
b = pi;
theta_f1 = (b-a).*rand(100,1) + a;
theta_f2 = (b-a).*rand(100,1) + a;
theta_f3 = (b-a).*rand(100,1) + a;
theta_f4 = (b-a).*rand(100,1) + a;
theta_f5 = (b-a).*rand(100,1) + a;

% Set values to test
d1 = 9;
a2 = 5;
a3 = 2;
d5 = 14;

failed_value = zeros(5,1);

for i = 1:100
    th_list = [theta_f1(i); theta_f2(i); theta_f3(i); theta_f4(i); theta_f5(i)];

    T_final = WinXFwd(th_list,d1,a2,a3,d5);

    % Calculate a list of theta values
    % Py and Px = d05
    % Might need to create some T matrix and grab the x and y values to store
    % Or generate some arbitrary values
    Py = T_final(2,4);
    Px = T_final(1,4);

    theta1 = atan2(Py, Px); % +-

    T1 = dhTransform(0, d1, -pi/2, theta1);

    z_0 = [0;0;1];

    d03 = T_final(1:3,4) - T_final(1:3,3)*d5;
    d13 = d03 - d1*z_0;

    d = T1(1:3,1:3)' * d13;
    d_13 = sqrt(d(1)^2 + d(2)^2);

    theta3 = 2*atan( sqrt( ((a2 + a3)^2 - d_13^2) / ( d_13^2 - (a2 - a3)^2) ));

    % Py, Px = d13
    theta2 = -atan2(-d(2), d(1)) + atan2(a3 * sin(theta3), a2 + a3*cos(theta3));


    T_03 = dhTransform(0, d1, -pi/2, theta1)*dhTransform(a2,0,pi,theta2)*...
        dhTransform(a3,0,0,theta3);
    T35 = T_03\T_final;
    d35 = T35(1:3,4);
    theta4 = atan2(-d35(1), d35(2));


    T_04 = T_03*dhTransform(0,0,-pi/2,theta4);

    R_45 = T_04(1:3,1:3)'*T_final(1:3,1:3);

    theta5 = atan2(R_45(2,1), R_45(1,1));
    
    if (theta1 ~= theta_f1(i)) || (theta2 ~= theta_f2(i)) || (theta3 ~= theta_f3(i)) ...
            || (theta4 ~= theta_f4(i)) || (theta5 ~= theta_f5(i))
        fprintf('Test at %f failed\n', i);
        fprintf('Expected: %f %f %f %f %f\n', theta_f1(i), theta_f2(i), theta_f3(i), theta_f4(i), theta_f5(i));
        fprintf('Actual: %f %f %f %f %f\n\n', theta1, theta2, theta3, theta4, theta5);
    end
end
