%% homework3 Jonny
%% Problem 4
%% Part A
% Rotation Matrix
close all
rot_final = [0.5000, -0.6124, 0.6124; 0.6124, 0.7500, 0.2500; -0.6124, 0.2500, 0.7500];
rot_init = [1, 0, 0; 0, 1, 0; 0, 0, 1];

full_transform = [0.5000, -0.6124, 0.6124, 0.8415; 0.6124, 0.7500, 0.2500, 0.3251; -0.6124, 0.2500, 0.7500, -0.3251; 0,0,0,1];

% Initial Angles
thetaInit = 0;
phiInit = 0;
psiInit = 0;

% Initial Displacements
displacementInitX = 0;
displacementInitY = 0;
displacementInitZ = 0;

% Final Displacements
displacementFinalX = 0.8415;
displacementFinalY = 0.3251;
displacementFinalZ = -0.3251;

% ZYZ Final: Cos, Sin
cThetaFinal = rot_final(3,3);
sThetaFinal = sqrt(rot_final(1,3)^2 + rot_final(2,3)^2);
cPhiFinal = rot_final(1,3)/sThetaFinal;
sPhiFinal = rot_final(2,3)/sThetaFinal;

% ZYZ Final Angles
phiFinal = atan2(sPhiFinal, cPhiFinal);
thetaFinal = atan2(sThetaFinal, cThetaFinal);
psiFinal = atan2(rot_final(3,2)/sThetaFinal, -rot_final(3,1)/sThetaFinal);

% Divide angles equally 6 times
phiSplit = phiFinal/6;
thetaSplit = thetaFinal/6;
psiSplit = psiFinal/6;

% Divide displacements by 6 to get the 6 locations
displacementSplitX = displacementFinalX/6;
displacementSplitY = displacementFinalY/6;
displacementSplitZ = displacementFinalZ/6;

xcurrentvect = zeros(1,6);
ycurrentvect = zeros(1,6);
zcurrentvect = zeros(1,6);

full_rotation = zeros(3,3,6);

Ux = zeros(1,6);
Vx = zeros(1,6);
Wx = zeros(1,6);
Uy = zeros(1,6);
Vy = zeros(1,6);
Wy = zeros(1,6);
Uz = zeros(1,6);
Vz = zeros(1,6);
Wz = zeros(1,6);
X = zeros(1,6);
Y = zeros(1,6);
Z = zeros(1,6);

Quaternion = zeros(6,4);

angles = zeros(3,6);

% Angle interpolations
for i = 1:6
    % Phi
    %angles(1,i) = ((phiSplit*i - phiInit)/((phiInit + phiSplit*i) - )) * (phiSplit - i) + phiInit;
    angles(1,i) = phiInit + i*((phiSplit*i) - phiInit);
    % Theta
    angles(2,i) = thetaInit + i*((thetaSplit*i) - thetaInit);
    % Psi
    angles(3,i) = psiInit + i*((psiSplit*i) - psiInit);
    
    phiInit = phiSplit*i;
    thetaInit = thetaSplit*i;
    psiInit = psiSplit*i;
end

% Calculate rotation matrices
for i = 1:6
    %rot = rotZ(phiSplit*i)*rotY(thetaSplit*i)*rotZ(psiSplit*i);
    full_rotation = rotZ(phiSplit*i)*rotY(thetaSplit*i)*rotZ(psiSplit*i);
    %full_rotation = rotZ(angles(1,i))*rotY(angles(2,i))*rotZ(angles(3,i));
    
    xcurrent = 1 * displacementSplitX + displacementInitX;
    ycurrent = 1 * displacementSplitY + displacementInitY;
    zcurrent = 1 * displacementSplitZ + displacementInitZ;
    
    
    % Set new initial values
    displacementInitX = displacementInitX + displacementSplitX;
    displacementInitY = displacementInitY + displacementSplitY;
    displacementInitZ = displacementInitZ + displacementSplitZ;
    xcurrentvect(i) = xcurrent;
    ycurrentvect(i) = ycurrent;
    zcurrentvect(i) = zcurrent;
    
    Ux(i) = full_rotation(1,1);
    Vx(i) = full_rotation(2,1);
    Wx(i) = full_rotation(3,1);
    Uy(i) = full_rotation(1,2);
    Vy(i) = full_rotation(2,2);
    Wy(i) = full_rotation(3,2);
    Uz(i) = full_rotation(1,3);
    Vz(i) = full_rotation(2,3);
    Wz(i) = full_rotation(3,3);
    
end

figure(1)
quiver3(xcurrentvect, ycurrentvect, zcurrentvect, Ux, Vx, Wx);
hold on
quiver3(xcurrentvect, ycurrentvect, zcurrentvect, Uy, Vy, Wy);
quiver3(xcurrentvect, ycurrentvect, zcurrentvect, Uz, Vz, Wz);
hold off

xlabel('X');
ylabel('Y');
zlabel('Z');


%% Part B Angle Axis
[k_hat, axisAngleTheta] = rot2AngleAxis(rot_final);

axisAngleThetaSplit = axisAngleTheta/6;

for i = 1:6
    % Theta
    angles(2,i) = thetaInit + i*((axisAngleThetaSplit*i) - thetaInit);
    
    thetaInit = axisAngleThetaSplit*i;
end


for i = 1:6
    axis_rot = angleAxis2Rot(k_hat, axisAngleThetaSplit*i);
    %axis_rot = angleAxis2Rot(k_hat, angles(2,i)*i);
    Ux(i) = axis_rot(1,1);
    Vx(i) = axis_rot(2,1);
    Wx(i) = axis_rot(3,1);
    
    Uy(i) = axis_rot(1,2);
    Vy(i) = axis_rot(2,2);
    Wy(i) = axis_rot(3,2);
    
    Uz(i) = axis_rot(1,3);
    Vz(i) = axis_rot(2,3);
    Wz(i) = axis_rot(3,3);
end

figure(2)
quiver3(xcurrentvect, ycurrentvect, zcurrentvect, Ux, Vx, Wx);
hold on
quiver3(xcurrentvect, ycurrentvect, zcurrentvect, Uy, Vy, Wy);
quiver3(xcurrentvect, ycurrentvect, zcurrentvect, Uz, Vz, Wz);
hold off

xlabel('X');
ylabel('Y');
zlabel('Z');


%% Part C Quaternions
Qf = rot2Quat(rot_final);
Qi = rot2Quat(rot_init);

alpha = acos(dot(Qi, Qf));

for i = 1:6
    q0 = (sin((1-(i/6))*alpha)/sin(alpha))*Qi + (sin((i/6)*alpha)/sin(alpha))*Qf;
    
    q0 = q0/norm(q0);
    
    quat_rot = quat2Rot(q0);
    
    Ux(i) = quat_rot(1,1);
    Vx(i) = quat_rot(2,1);
    Wx(i) = quat_rot(3,1);
    
    Uy(i) = quat_rot(1,2);
    Vy(i) = quat_rot(2,2);
    Wy(i) = quat_rot(3,2);
    
    Uz(i) = quat_rot(1,3);
    Vz(i) = quat_rot(2,3);
    Wz(i) = quat_rot(3,3);
    
end

figure(3)
quiver3(xcurrentvect, ycurrentvect, zcurrentvect, Ux, Vx, Wx);
hold on
quiver3(xcurrentvect, ycurrentvect, zcurrentvect, Uy, Vy, Wy);
quiver3(xcurrentvect, ycurrentvect, zcurrentvect, Uz, Vz, Wz);
hold off
xlabel('X');
ylabel('Y');
zlabel('Z');


%% Part D Twist

twist = transform2Twist(full_transform);
twist_step = twist/6;
for i = 1:6
    trans_step = twist2Transform(twist_step*i);
    
    disp_twist = trans_step(1:3,4);
    
    xcurrentvect(i) = disp_twist(1);
    ycurrentvect(i) = disp_twist(2);
    zcurrentvect(i) = disp_twist(3);
    
    Ux(i) = trans_step(1,1);
    Vx(i) = trans_step(2,1);
    Wx(i) = trans_step(3,1);
    
    Uy(i) = trans_step(1,2);
    Vy(i) = trans_step(2,2);
    Wy(i) = trans_step(3,2);
    
    Uz(i) = trans_step(1,3);
    Vz(i) = trans_step(2,3);
    Wz(i) = trans_step(3,3);
end

figure(4)
quiver3(xcurrentvect, ycurrentvect, zcurrentvect, Ux, Vx, Wx);
hold on
quiver3(xcurrentvect, ycurrentvect, zcurrentvect, Uy, Vy, Wy);
quiver3(xcurrentvect, ycurrentvect, zcurrentvect, Uz, Vz, Wz);
hold off
xlabel('X');
ylabel('Y');
zlabel('Z');

