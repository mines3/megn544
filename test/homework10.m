%% MEG544 Homework 10
%% Jonny Wong

%% Part A
density = 2699; %kg/m^3
height = 0.005; % m
radius = 0.2; % m
mass = density * pi * radius^2 * height;
w = [0;0;1];
R_0 = [0.964 0.261 0.049; -0.239 0.933 -0.268; -0.116 0.246 0.962];

I_b =   [ 1/12*mass*(3*radius^2 + height^2) 0 0; ...
          0 1/12*mass*(3*radius^2 + height^2) 0;
          0 0 1/2*mass*radius^2]
      
%% Part B
I_w = R_0 * I_b * R_0'

%% Part C
alpha = - cross(w, I_w*w)\I_w
r_com = [0, 0, 0.0025] * R_0;
a = - (alpha.*r_com + cross(w, cross(w, r_com)))

%% Part D
acc = [.1 .2 .3]/mass

alpha = ([.4 .5 .6] - cross(w, I_w*w))/I_w