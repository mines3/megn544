syms L1 L2 th1 th2 th3 pii

%% Part A & B
a = [0; L1; L2];
d = [1; 0; 0];
alpha = [pii/2; 0; 0];
theta = [th1; th2; th3];

T03 = dhTransform(a(1), d(1), alpha(1), theta(1)) * dhTransform(a(2), d(2), alpha(2), theta(2)) * dhTransform(a(3), d(3), alpha(3), theta(3));

T03 = subs(T03, pii, pi);
P03 = T03(1:3, 4);
P03 = simplify(P03)

P = [diff(P03, th1) diff(P03, th2) diff(P03, th3)];
P = simplify(P)

%% Part C
T01 = dhTransform(a(1), d(1), alpha(1), theta(1));
T02 = dhTransform(a(1), d(1), alpha(1), theta(1)) * dhTransform(a(2), d(2), alpha(2), theta(2));

Z0 = T01(1:3, 3);
Z1 = T02(1:3, 3);
Z2 = T03(1:3, 3);

d03 = T03(1:3, 4);
d13 = d03 - T01(1:3, 4);
d23 = T03(1:3, 4) - T02(1:3, 4);

Jv = [cross([0;0;1], d03) cross(Z0, d13) cross(Z1, d23)];
Jv = simplify(Jv)

%% Part D
P = subs(P, [L1 L2 th1 th2 th3 pii], [1 .5 0 pi/4 pi/4 pi]);
Jv = subs(Jv, [L1 L2 th1 th2 th3 pii], [1 .5 0 pi/4 pi/4 pi]);

Jv_diff = P - Jv

