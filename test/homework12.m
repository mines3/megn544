%% Homework 12
% Jonny Wong
% MEGN544


%% Part 1
A = [0 1; 1 1];
B = [0; 1];
open_loop_poles = eig(A)

%% Part 2
Tr = .1;
Ts = 0.5;
os = 0.05; 


zeta = sqrt(log(os)^2/(pi^2+log(os)^2))
wn = max(4.6/(Ts*.9),(1.53*zeta + 2.31*zeta^3)/(Tr*.9))
sigma = zeta*wn

%poles = -sigma*([1;1] + [sqrt(1-zeta^2)/zeta*1i;-sqrt(1-zeta^2)/zeta *1i])
%poles = @(sig,z,t) 1 - 1/(sqrt(1-z^2)).*exp(-sig*t).*sin(sig*sqrt(1-z^2)/z*t + acos(z));
%poles = @(t) 1 - 1/(sqrt(1-zeta^2)).*exp(-sigma*t).*sin(sigma*sqrt(1-zeta^2)/zeta*t + acos(zeta));

time = linspace(0,1,100);
K = place(A,B,poles)

Bss = (B'*B)^-1*B'*A


%figure(1)
%plot(time,poles(time))




