%% Jonny Wong
%% Homework 11

%% Part 1 -- Transformation Matrices

a = [0; 0.5; 0.25];
d = [0.5; 0; 0];
theta_dot = [-2.5420; -1.0181; -0.9636];
theta_ddot = [8.4824; -2.1357; -0.5438];
alpha = [pi/2; 0; -pi/2];
theta = [pi/6; pi/5; pi/4];
offset = [0;0;0];
rcom = {[0; -0.25; 0];[-0.25; 0; 0];[-0.125; 0; 0]}; 
mass = [31.6; 31.6; 15.8]; % kg

inertia = {[0.6783 0 0; 0 0.0395 0; 0 0 0.6783]; [0.0395 0 0; 0 0.6783 0; 0 0 0.6783]; ...
    [0.0198 0 0; 0 0.0922 0; 0 0 0.0922]};

linkList1 = createLink(a(1), d(1), alpha(1), [], offset(1), rcom{1,:,:}, mass(1), inertia{1,:,:});
linkList2 = createLink(a(2), d(2), alpha(2), [], offset(2), rcom{2,:,:}, mass(2), inertia{2,:,:});
linkList3 = createLink(a(3), d(3), alpha(3), [], offset(3), rcom{3,:,:}, mass(3), inertia{3,:,:});

linkList = [linkList1, linkList2, linkList3];

linkSize = 3;
T = zeros(4,4,linkSize);
R = zeros(3,3,linkSize);
T_init = eye(4);

for i = 1:3
    T(:,:,i) = T_init * dhTransform(a(i), d(i), alpha(i), theta(i))
    T_init = T(:,:,i);
end



%d_dot = zeros(linkSize,1);
%d_ddot = zeros(linkSize,1);
%inertia = zeros(3,3,linkSize);
J = zeros(3,3,linkSize);
%rcom = zeros(3,linkSize); %% com of the link with respect to link
rcom_base = zeros(3,linkSize); %% com of the link with respect to base
%mass= zeros(linkSize,1); %% mass of lik
jointTorques = zeros(3,3);
Z = zeros(3,linkSize);
dd = zeros(3,linkSize+1); % This is D
dd_dot = zeros(3,linkSize+1);
dd(:,1:1) = [0; 0; 0];
Z(:,1) = [0 0 1];
H = eye(4);

w = zeros(3,linkSize+1);
w_dot = zeros(3,linkSize+1);
f = zeros(3,linkSize+1);
n = zeros(3,linkSize+1);
dd_ddot = zeros(3,linkSize+1);

%dd_ddot(:,1:1) = boundary_conditions(1).base_linear_acceleration; % Solve for this value
dd_ddot(:,1) = [0;0;9.81];



for i = 2:linkSize+1
    dd(:,i:i) = T(1:3,4:4,i-1);
    Z(:,i) = T(1:3,3:3,i-1);
    R(:,:,i-1) = T(1:3,1:3,i-1);
end

for i = 1:linkSize
    J(:,:,i) = R(:,:,i)*inertia{i,:,:}*R(:,:,i)';
end

%% Part 3 -- Angular Velocity and Acceleration
% Solve for w and w_dot -- Angular velocity & angular acceleration
for i = 2:linkSize+1
    w(:,i) = w(:,i-1) + theta_dot(i-1)*Z(:,i-1)
    w_dot(:,i) = w_dot(:,i-1) + theta_ddot(i-1)*Z(:,i-1) + cross(theta_dot(i-1)*w(:,i-1), Z(:,i-1))
end

%% Part 2 -- Linear velocity & acceleration
% Solve for D dot -- Linear Velocity
dd_dot(:,1:1) = [0; 0; 0];
for i = 2:linkSize+1
    dd_dot(:,i) = dd_dot(:,i-1) + cross(w(:,i),(dd(:,i)-dd(:,i-1)))
end

% Solve for D double dot -- Linear Acceleration
for i = 2:linkSize+1 % D double dot
    dd_ddot(:,i:i) = dd_ddot(:,i-1) + cross(w_dot(:,i),(dd(:,i)-dd(:,i-1))) + ...
        cross(w(:,i),cross(w(:,i),(dd(:,i)-dd(:,i-1))))
end

%% Part 4 -- Rcom velocity and acceleration
% Solve for rcom base
for i = 1:linkSize
    rcom_base(:,i:i) = R(:,:,i)*rcom{i,:,:};
end

%Solve for rcom dot -- Linear Velocity
rcom_base_dot = zeros(3,linkSize);
for i = 1:linkSize
    rcom_base_dot(:,i) = dd_dot(:,i) + cross(w(:,i+1),rcom{i,:,:})
end

% Solve for rcom double dot -- Linear Acceleration
rcom_base_ddot = zeros(3,linkSize);
for i = 1:linkSize
    rcom_base_ddot(:,i) = dd_ddot(:,i) + cross(w_dot(:,i+1),rcom_base(:,i)) + ...
        cross(w(:,i+1),cross(w(:,i+1),rcom_base(:,i)))
end

%% Part 5-8 -- Inertial forces, torques and jointTorques
for i = linkSize:-1:1
    %f(:,i:i) = f(:,i+1) + mass(i)*(rcom_base_ddot(:,i));
    %n(:,i:i) = n(:,i+1) + cross(dd(:,i+1)-dd(:,i)+rcom_base(:,i),f(:,i)) + J(:,:,i)*w_dot(:,i+1) + ...
    %    cross(w(:,i+1),J(:,:,i)*w(:,i+1)) - cross(rcom_base(:,i),f(:,i+1));
    f(:,i) = f(:,i+1) + mass(i)*(rcom_base_ddot(:,i) + cross(w(:,i+1), cross(w(:,i+1), rcom_base(:,i)))) + ...
        alpha(i)*rcom_base(:,i) + cross(2*w(:,i), rcom_base_dot(:,i))
    n(:,i) = n(:,i+1) + J(:,:,i)*w(:,i) + cross(w(:,i), J(:,:,i)*w(:,i))
        
    jointTorques(:,i) = Z(:,i).*n(:,i)
end


[Jv,JvDot] = velocityJacobian(linkList,theta,theta_dot)
