import os
import sys
import numpy as np
from numpy import linalg as la
import math
from rotation.RotationXYZ import Rotation
from scipy.spatial.transform import Rotation as R
#from scipy.spatial.transform import Slerp
from scipy.linalg import expm, sinm, cosm
sys.path.append(".")

def calculate_twist(expo, theta, k_hat, vect):
    print(expo)
    k_hat_cross = np.array([
            [0,             -k_hat[2,0],    k_hat[1,0]  ],
            [k_hat[2,0],    0,             -k_hat[0,0]  ],
            [-k_hat[1,0],   k_hat[0,0],     0]          ])
    """ k_hat_cross = np.array([
            [0, 0, 0],
            [0, 0, -k_hat[0,0]],
            [0, k_hat[0,0], 0]]) """

    twist = ((np.identity(3) - expo)*k_hat_cross + theta*k_hat*np.transpose(k_hat)).dot(vect)

    print(twist)

def main():

    omega_1a = np.array([
                [math.pi/3],
                [0],
                [0]])

    v_1a = np.array([
                [3/math.pi],
                [0],
                [0]])

    omega_cross_1a = np.array([
            [0, 0, 0],
            [0, 0, -math.pi/3],
            [0, math.pi/3, 0]])

    theta = la.norm(omega_1a)

    expo = expm(omega_cross_1a)

    k_hat = omega_1a/theta

    calculate_twist(expo, theta, k_hat, v_1a)

    omega_2a = np.array([
                [0],
                [math.pi/2],
                [0]])

    v_2a = np.array([
                [0],
                [0.5],
                [0]])

    omega_cross_2a = np.array([
            [0, 0, math.pi/2],
            [0, 0, 0],
            [-math.pi/2, 0, 0]])

    theta = la.norm(omega_2a)

    expo = expm(omega_cross_2a)

    k_hat = omega_2a/theta

    calculate_twist(expo, theta, k_hat, v_2a)

    omega_3a = np.array([
                [0],
                [0],
                [math.pi/3]])

    v_3a = np.array([
                [1],
                [0],
                [0]])

    omega_cross_3a = np.array([
            [0, -math.pi/3, 0],
            [math.pi/3, 0, 0],
            [0, 0, 0]])

    theta = la.norm(omega_3a)

    expo = expm(omega_cross_3a)

    k_hat = omega_3a/theta

    calculate_twist(expo, theta, k_hat, v_3a)

    omega_2d = np.array([
                [0],
                [.34],
                [.34]])

    omega_cross_2d = np.array([
            [0, -.726, .726],
            [.726, 0, 0],
            [.726, 0, 0]])

    theta = 1.25

    expo = expm(omega_cross_2d)

    k_hat = np.array([
                [0],
                [.5811],
                [.5811]])

    k_hat_cross = np.array([
            [0, -.5811, .5811],
            [.5811, 0, 0],
            [.5811, 0, 0]])

    displ = np.array([
                [0.8415],
                [0.3251],
                [-0.3251]])

    v_2d = ((math.sin(theta)/(2*(1-math.cos(theta))))*np.identity(3) + ((2*(1-math.cos(theta)) -theta*math.sin(theta))/(2*theta*1-math.cos(theta)) *k_hat*np.transpose(k_hat)) - .5*k_hat_cross).dot(displ)

    calculate_twist(expo, theta, k_hat, v_2d)


    """
        Question 4
    """

"""     rot_final = R.from_matrix([
                [0.5000, -0.6124, 0.6124],
                [0.6124, 0.7500, 0.2500],
                [-0.6124, 0.2500, 0.7500]])

    key_times = [0, 1, 2, 3, 4]

    slerp = Slerp(key_times, rot_final)

    times = [0, 1, 2, 3, 4]
    interp_rots = slerp(times)

    print(rot_final.as_euler('zyz', degrees=True))
    interp_rots.as_euler('zyz', degrees=True)

    print(interp_rots) """




if __name__ == "__main__":
    main()
