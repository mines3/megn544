import os
import numpy as np
import math

def main():
    # 3a
    matrix_a = np.array([
                [1, 0, 0, 2],
                [0, 1, 0, 1],
                [0,0,1,0],
                [0,0,0,1]])
    # 3b
    matrix_b = np.array([
                [math.cos(math.pi/4), -math.sin(math.pi/4), 0, 0],
                [math.sin(math.pi/4), math.cos(math.pi/4), 0, 0],
                [0,0,1,0],
                [0,0,0,1]])
    # 3c
    matrix_c = np.array([
                [1, 0, 0, 2],
                [0, 1, 0, 0],
                [0,0,1,0],
                [0,0,0,1]])
    # 3d
    matrix_d = np.array([
                [math.cos((3*math.pi)/4), -math.sin((3*math.pi)/4), 0, 0],
                [math.sin((3*math.pi)/4), math.cos((3*math.pi)/4), 0, 0],
                [0,0,1,0],
                [0,0,0,1]])
    # 3e
    matrix_e = np.array([
                [1, 0, 0, 0],
                [0, 1, 0, 3],
                [0,0,1,0],
                [0,0,0,1]])

    # 3f
    final = matrix_a.dot(matrix_b).dot(matrix_c).dot(matrix_d).dot(matrix_e)
    print("3F Matrix: \n{}".format(final))

    # 3i
    final = matrix_b.dot(matrix_c).dot(matrix_d).dot(matrix_e)
    print("3I Matrix: \n{}".format(final))
    print("Inverse: \n{}".format(np.linalg.inv(np.matrix(final))))

    # 4b
    matrix_f = np.array([
                [0, 1, 0],
                [1, 0, 0],
                [0, 0, -1]])
    print("4B: \n{}".format(np.linalg.det(matrix_f)))
    # 4c
    matrix_g = np.array([
                [-.254, 0.087, -.963],
                [.967, 0.0169, -0.253],
                [-0.00572, -0.996, -0.0884]])
    print("4C: \n{}".format(np.linalg.det(matrix_g)))
    # 4d
    matrix_h = np.array([
                [.546, 0.0719, -1.47],
                [0.814, -0.696, -0.0502],
                [-1.23, -0.813, .474]])
    print("4D: \n{}".format(np.linalg.det(matrix_h)))
    # 5b
    matrix_i = np.array([
                [-1, 0, 0],
                [0, 1, 0],
                [0, 0, -1]])
    print("5B: \n{}".format(np.linalg.det(matrix_i)))
    # 5c
    matrix_j = np.array([
                [0.3698, 0.3352, 0.8665],
                [-0.9276, 0.185963, 0.324],
                [-0.05236, -0.92362, 0.3797]])
    print("5C: \n{}".format(np.linalg.det(matrix_j)))
    # 5d
    matrix_k = np.array([
                [0.409386, 0.1166, 0.904881],
                [-0.2039, -0.955023, 0.2153],
                [0.889285, -0.272645, -0.3672]])
    print("5D: \n{}".format(np.linalg.det(matrix_k)))


if __name__ == "__main__":
    main()
