%CREATELINK 
%   Input: 
%       a - Perpendicular distance from Zi-1 to Zi along Xi
%       d - Perpendicular distance between Xi-1 to Xi along Zi-1
%       alpha - Angle from Zi-1 to Zi along Xi
%       theta - Angle from Xi-1 to Xi along Zi-1
%       offset - The number of radians (or meters for
%                prismatic) different between the encoder 
%                orientation and the DH zero-angle. θ_DH = Θ - Θ_offset 
%                (equivalent equation for prismatic).
%       centOfMass - The position of the link’s center of mass
%       mass - Link mass (kg)
%       inertia - link mass moment of inertia (kg m^2)
%   Output: 
%       L - Struct of inputs
%   Dependent Methods: None
%   Description:
%       Function that creates a struct that contains a, d, alpha, theta, 
%       offset, com, mass, inertia and isRotary
%
% Jonny Wong
% CWID: 10884664
% MEGN544
% Oct 24, 2021
%
function [L] = createLink(a, d, alpha, theta, offset, centOfMass, mass, inertia)

L.a = a;
L.d = d;
L.alpha = alpha;
L.theta = theta;
L.offset = offset;
L.com = centOfMass;
L.mass = mass;
L.inertia = inertia;

if isempty(theta)
    L.isRotary = 1;
elseif isempty(d)
    L.isRotary = 0;
else
    L.isRotary = -1;
end

end
