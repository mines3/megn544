%CPMAP
%   Input:
%       W - 3x1 Matrix
%   Output:
%       crossProductMap - 3x3 Cross Product Mapping
%   Dependent Methods: None
%   Description:
%       Function that receives 3x1 Matrix(W) and maps the matrix to a 3x3 matrix
%
% Jonny Wong
% CWID: 10884664
% MEGN544
% Sept 22, 2021
%
function [crossProductMap] = cpMap(W)

crossProductMap =   [0 -W(3) W(2);
                     W(3) 0 -W(1);
                    -W(2) W(1) 0];
end

