%ROTATIONERROR 
%   Input: 
%       Rot_desired - Rotation matrix of the desired rotation
%
%       Rot_current - Rotation matrix of the current rotation
%
%   Output: 
%       rot_error_vector - A vector of errors for rotation matrix error.
%
%   Dependent Methods: rot2AngleAxis
%
%   Description:
%       Function that receives a desired and current rotation
%       matrix and calculates the error between the two. The function take
%       the current rotation and transposes the value and multiplies it by
%       the desired. It then takes the error and passes it through the
%       rot2AngleAxis function to get the k and theta value. It then
%       multiplies the k, theta and current rotation to get the rotation
%       error vector. 
%
%
%
%
%
%
%
%
% Jonny Wong
% CWID: 10884664
% MEGN544
% Nov 14, 2021
%
function [rot_error_vector] = rotationError(Rot_desired, Rot_current)
    error = Rot_desired * Rot_current';
    [~, ~, omega]= rot2AngleAxis(error);
    rot_error_vector = omega;
end

