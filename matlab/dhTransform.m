%DHTRANSFORM 
%   Input: 
%       a - Perpendicular distance from Zi-1 to Zi along Xi
%       d - Perpendicular distance between Xi-1 to Xi along Zi-1
%       alpha - Angle from Zi-1 to Zi along Xi
%       theta - Angle from Xi-1 to Xi along Zi-1
%   Output: 
%       H - Homogeneous Transformation Matrix of a dhTransform
%   Dependent Methods: rotX, rotZ
%   Description:
%       Function that receives an a, d, alpha and theta value and returns
%       a dh Transformation Matrix
%
% Jonny Wong
% CWID: 10884664
% MEGN544
% Sept 22, 2021
%
function [H] = dhTransform(a, d, alpha, theta)

R_x=rotX(alpha);
R_z=rotZ(theta);
H_z=[R_z [0; 0; d]; 0 0 0 1];
H_x=[R_x [a; 0; 0]; 0 0 0 1];
H=H_z*H_x;
end

