%TRANSERROR 
%   Input: 
%       Td - Homogeneous Transformation matrix of the desired
%            transformation position.
%       Tc - Homogeneous Transformation matrix of the current
%            transformation position.
%   Output: 
%       error_vector - A vector of errors for position and rotation matrix
%                      error.
%   Dependent Methods: rotationError
%   Description:
%       Function that receives a desired and current homogeneous
%       transformation matrix and calculates the error between the two.
%
%
%
%
%
%
%
%
% Jonny Wong
% CWID: 10884664
% MEGN544
% Sept 22, 2021
%
function [error_vector] = transError(Td, Tc)
    Rd=Td(1:3,1:3);
    Rc=Tc(1:3,1:3);
    rot_error = rotationError(Rd, Rc);
    pos_error=Td(1:3,4)-Tc(1:3,4);
    error_vector=[pos_error;rot_error];
end

