clear; clc; close all;

%% Set Simulation Parameters
enable_control = true;
drawCSM = true;  % Draws CSM on the plot, requires a points3D.mat file to exist
Sim_Exact = false; % sets if the simulated geometry is exact (true) or slightly different (false)
simTime = 10; %Run the simulation for the specified number of seconds
makeMovie = false;
Laser_Feedback = false;
plot_arm = false; % controls if the arm is plotted during simulation. Slows down simulink a bit... 


%% verify necessary functions exist
assert(exist('velocityJacobian.m','file')==2,'Simulation Error:  Need to add project files to path');
assert(exist('transError.m','file')==2,'Simulation Error:  Need to add project files to path');
assert(exist('cpMap.m','file')==2,'Simulation Error:  Need to add project files to path');
assert(exist('newtonEuler.m','file')==2,'Simulation Error:  Need to add project files to path');
assert(exist('dhFwdKine.m','file')==2,'Simulation Error:  Need to add project files to path');
assert(exist('constAccelInterp.m','file')==2,'Simulation Error:  Need to add project files to path');
assert(exist('createLink.m','file')==2,'Simulation Error:  Need to add project files to path');


%% Controller Parameter Definitions
load Trajectory_File.mat;
run Geometry.m
trajectory=zeros(max(size(transform_list))+6,8);
for i=1:max(size(transform_list))
    trans=transform_list{i};
    trajectory(i+3,2:4)=points3D(i,:)';
    Q = rot2Quat(trans(1:3,1:3));
    if dot(trajectory(i+2,5:8), Q) < 0
        Q = -Q;
    end
    Q = Q/norm(Q);
    trajectory(i+3,5:8)=Q;
end

% Starting config -- start
paramList = [0 pi/2 0 0 0 0];
H = dhFwdKine(linkList, paramList);
Quat = rot2Quat(H(1:3,1:3));
Quat = Quat/norm(Quat);

if dot(trajectory(3,5:8), Quat) < 0
    Quat = -Quat;
end
trajectory(1,2:4) = H(1:3, 4);
trajectory(2,2:4) = H(1:3, 4);
trajectory(1,5:8) = Quat;
trajectory(2,5:8) = Quat;

% zero angle config -- end
paramList = [0 0 0 0 0 0];
H = dhFwdKine(linkList, paramList);
Quat = rot2Quat(H(1:3,1:3));
Quat = Quat/norm(Quat);

if dot(trajectory(end-1,5:8), Quat) < 0
    Quat = -Quat;
end

trajectory(end,2:4)   = H(1:3, 4);
trajectory(end-1,2:4) = H(1:3, 4);
trajectory(end-2,:) = trajectory(end-3,:);
trajectory(end,5:8) = Quat;
trajectory(end-1,5:8) = Quat;

% Duplicate position 4 into 3 -- start of C
trajectory(3, :) = trajectory(4,:);

for j=2:max(size(transform_list))
    if dot(trajectory(j,5:8),trajectory(j-1,5:8))<0
        trajectory(j,5:8)=-trajectory(j,5:8);
    end
end
t=linspace(3,simTime,max(size(transform_list))+4)';
trajectory(1, 1) = 0;
trajectory(2, 1) = 0.2500;
trajectory(3:end,1)=t;


A = [0 1;0 0];
B = [0;1];
Tp = .13; % desired peak time; ORIG: 0.18
[~,poles_b,~] = besself(size(A,1),2*pi/Tp);
Kp = place(A,B,poles_b);


%% zoh sys
dt = .001;
tmp = [A B;0 0 0];
tmp_d = expm(tmp*dt);

Ad = tmp_d(1:2,1:2);
Bd = tmp_d(1:2,3);

Pd = exp(poles_b*dt);

Kd = place(Ad,Bd,Pd);

Kp = Kp(1);
Kd = Kd(1);

%% Define Kp and Kd gains
% Remember that the control update rate is 0.001 seconds
% Remember that the feedback linearization makes the system look like a
% pure integrator that is: A = [0 1; 0 0] and B = [0; 1]
%Kp = 300; % you need to update these in a meaningful way
%Kd = 100; % you need to update these in a meaningful way

%% Define Your Trajectory. I like to make a makeTrajectory function to help
% trajectory = makeTrajectory( simTime, ramp_time);   
% trajectory = [];
trans_percent = .1; % ORIG 0.05

%% Controller Parameter Definitions
Sim_Name = 'System_InvDynamics';
%run Geometry.m; % creates a linkList in the workspace for you and for the simulation
if drawCSM
    sizeCSM = size(points3D,1);
else
    sizeCSM = 0;
end
open([Sim_Name '.slx']);


%% Enable/Disable Laser Feedback
set_param([Sim_Name '/Laser_Feedback'],'sw', int2str(Laser_Feedback));

%% Set Gains
set_param([Sim_Name '/Kp'],'Gain',['[' num2str(reshape(Kp,[1,numel(Kp)])) ']']);
set_param([Sim_Name '/Kd'],'Gain',['[' num2str(reshape(Kd,[1,numel(Kd)])) ']']);

%% Choose Simulation System (perfect model or realistic model)
set_param([Sim_Name '/ABB Arm Dynamics/sim_exact'], 'sw', int2str(Sim_Exact))

%% Enable/Disable Control
set_param([Sim_Name '/control_enable'], 'sw', int2str(enable_control))

%% Run Simulation
simOut =  sim(Sim_Name,'SimulationMode','normal','AbsTol','1e-5','StopTime', int2str(simTime),...
    'SaveState','on','StateSaveName','xout',...
    'SaveOutput','on','OutputSaveName','yout',...
    'SaveFormat', 'array');


%% Extract Variables From Simulation
laser_tracking = simOut.get('laser_tracking');
theta_dot_actual = simOut.get('theta_actual');
theta_actual = simOut.get('theta_actual');
control_torque = simOut.get('control_torque');
pose_error = simOut.get('pose_error');

%% Plot theta as a function of time
figure(1)
for i=1:6
    subplot(3,2,i)
    plot(theta_actual.time,theta_actual.signals.values(:,i))
    title(['\theta_', int2str(i)])
    xlabel('time (s)')
    ylabel('angle (rad)')
    grid on;
end
saveas(gcf,'Theta Set','jpg')

figure(3)
subplot(2,1,1)
semilogy(pose_error.time,sqrt(sum(pose_error.signals.values(:,1:3).^2,2)));
title(['Position Pose Error']);
xlabel('time (s)');
ylabel('Norm Error (m)');
grid on;
subplot(2,1,2)
semilogy(pose_error.time,sqrt(sum(pose_error.signals.values(:,4:6).^2,2)));
title(['Angular Pose Error']);
xlabel('time (s)');
ylabel('Norm Error (rad)');
grid on;
saveas(gcf,'Pose Error','jpg')

figure(4)
subplot(2,1,1)
plot(control_torque.time,control_torque.signals.values);
title(['Control Torque Requested - Saturation Region']);
xlabel('time (s)');
ylabel('Torque [N-m]');
ylim([-350,350])
grid on;
subplot(2,1,2)
plot(control_torque.time,control_torque.signals.values);
title(['Control Torque Requested - Full Scale']);
xlabel('time (s)');
ylabel('Torque [N-m]');
grid on;
saveas(gcf,'Control Torque','jpg')


%% Display Arm Motion Movie

if makeMovie
    obj = VideoWriter('proj_5_arm_motion','MPEG-4');
    obj.FrameRate = 30;
    obj.Quality = 50;
    obj.open();
end

figure(2)
plot(0,0); ah = gca; % just need a current axis handel
fh = gcf;
stepSize = fix((1/30)/theta_actual.time(2)); % 30 frames per second
for i=1:stepSize:length(theta_actual.time)
    plotArm(theta_actual.signals.values(i,:),Sim_Exact,ah);
    hold on;
    plot3(reshape(laser_tracking.signals.values(1,4,1:i),[i,1]),... % x pos
        reshape(laser_tracking.signals.values(2,4,1:i),[i,1]),... % y pos
        reshape(laser_tracking.signals.values(3,4,1:i),[i,1]),'r','Parent',ah); % z pos
    if drawCSM
        plot3(points3D(:,1),points3D(:,2),points3D(:,3),'m','Parent',ah);
    end
    hold off;
    if makeMovie
        obj.writeVideo(getframe(fh));
    end
    pause(1/30)
end
if makeMovie
    obj.close();
end

saveas(gcf,'Trajectory','jpg')
