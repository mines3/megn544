%RPY2ROT
%   Input: 
%       Roll - Scalar value in radians
%       Pitch - Scalar value in radians
%       Yaw - Scalar value in radians
%   Output: 
%       rotationM - Rotation matrix of an ZYX rotation
%   Dependent Methods: rotX, rotY, rotZ
%   Description:
%       This function multiplies the ZYX rotation matrices with the input
%       values of roll, pitch, yaw respectively
%
% Jonny Wong
% CWID: 10884664
% MEGN544
% Sept 22, 2021
%
function [rotationM] = rpy2Rot(roll, pitch, yaw)

rotationM = rotZ(yaw)*rotY(pitch)*rotX(roll);

end

