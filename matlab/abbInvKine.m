%ABBINVKINE 
%   Input: 
%       T_des - Final destination of Homogeneous Transformation Matrix
%       th_last - Array of theta values in the last position
%   Output: 
%       th1 - Theta 1 value
%       th2 - Theta 2 value
%       th3 - Theta 3 value
%       th4 - Theta 4 value
%       th5 - Theta 5 value
%       th6 - Theta 6 value
%       reachable - Boolean whether thetas were real of imaginary
%   Dependent Methods: dhTransform
%   Description:
%       Function that calculates the inverse kinematics of a 6 DOF ABB ARM
%
% Jonny Wong
% CWID: 10884664
% MEGN544
% Oct 24, 2021
%
function [th1,th2,th3,th4,th5,th6 , reachable] = abbInvKine (T_des, th_last)
    a = [0; 0.27; 0.07; 0; 0; 0];
    d = [0.290; 0; 0; 0.302; 0; 0.072];
    alpha = [-pi/2; 0; -pi/2; pi/2; -pi/2; 0];
    offset = [0; pi/2; 0; 0; 0; 0];
    I = eye(3);
    
    h = sqrt(a(3)^2 + d(4)^2);
    
    % Wrist Position
    d_05 = T_des(1:3,4) - (d(6) * T_des(1:3,3));
    
    % Vector comp that represent wrist position
    d_05_x = d_05(1,1);
    d_05_y = d_05(2,1);
    
    %% Theta1 
    theta1(1) = atan2(d_05_y, d_05_x);
    if d_05_y >= 0
        theta1(2) = atan2(d_05_y, d_05_x) - pi;
    elseif d_05_y < 0
        theta1(2) = atan2(d_05_y, d_05_x) + pi;
    end
    
    if (abs(th_last(1) - theta1(1)) > abs(th_last(1) - theta1(2)))
        th1 = theta1(2);
    else
        th1 = theta1(1);
    end
    
    
    %% Theta 2 & 3
    T01 = dhTransform(a(1), d(1), alpha(1), th1);
    d14 = T01(1:3,1:3)' * (d_05 - (I(1:3,3) * d(1))); % Also d15
    sth3 = sqrt((2 * a(2) * h) + (d14(1,1)^2) + (d14(2,1)^2) - (a(2)^2) - (h^2));
    cth3 = real(sqrt((2 * a(2) * h) - (d14(1,1)^2) - (d14(2,1)^2) + (a(2)^2) + (h^2)));
    
    count = 0;
    if isreal(sqrt((2 * a(2) * h) - (d14(1,1)^2) - (d14(2,1)^2) + (a(2)^2) + (h^2))) == 0
        count = 1;
    end
    
    delta = atan2(d(4)/h, a(3)/h);
    
    % Theta 3
    if count == 0
        theta3(1) = pi - (2 * atan2(sth3,cth3)) - delta;
        theta3(2) = pi + (2 * atan2(sth3,cth3)) - delta;

        if theta3(1) < pi
            theta3(1) = theta3(1) + (2*pi);
        end
        if theta3(2) < pi
            theta3(2) = theta3(2) + (2*pi);
        end
        if theta3(1) > pi
            theta3(1) = theta3(1) - (2*pi);
        end
        if theta3(2) > pi
            theta3(2) = theta3(2) - (2*pi);
        end
    else
        theta3 = pi + (2 * atan2(sth3,cth3)) - delta;
    end
    [M3,I3] = min((th_last(3)-theta3).^2);
    th3 = theta3(I3);
    
    % Theta 2
    theta2 = atan2(d14(2), d14(1)) - atan2(h*sin(th3 + delta), a(2) + h*cos(th3 + delta)) + offset(2);
    
    if theta2 < pi
        theta2 = theta2 + (2*pi);
    end
    
    if theta2 > pi
        theta2 = theta2 - (2*pi);
    end
    th2 = theta2;

    %% Theta 4,5,6
    T03 = dhTransform(a(1), d(1), alpha(1), th1) * ...
        dhTransform(a(2), d(2), alpha(2), th2 - offset(2) - (2*pi)) * ...
        dhTransform(a(3), d(3), alpha(3), th3 - (2*pi));
    
    R36 = T03(1:3, 1:3)' * T_des(1:3,1:3);
    
    theta5(1) = atan2(sqrt((R36(3,1)^2) + (R36(3,2)^2)), R36(3,3));
    theta5(2) = atan2(-sqrt((R36(3,1)^2) + (R36(3,2)^2)), R36(3,3));
    
    if theta5(1) < -pi || theta5(2) < -pi
        theta5(1) = theta5(1) + (2*pi);
    end
    
    if theta5(1) > pi || theta5(2) > pi
        theta5(1) = theta5(1) - (2*pi);
    end
    
    % 1% error here
    [M5,I5] = min((th_last(5) - theta5).^2);
    t5 = theta5(I5);
    
    
    if round(sin(t5),2)==0
        if round(cos(t5),2)==1
            the5(1)=t5;
            the5(2)=t5+(2*pi);
            [M5_1,I5_1] = min((th_last(5)-the5).^2);
            th5 = the5(I5_1);
            
            Q = [2 0 1;0 2 1;1 1 0]\[2*th_last(4);2*th_last(6);atan2(R36(2,1),R36(1,1))];
            the4(1) = Q(1);
            the4(2) = Q(1) - pi;
            [M4_1,I4_1] = min((th_last(4)-the4).^2);
            th4 = the4(I4_1);
            the6(1) = Q(2);
            the6(2) = Q(2) - pi;
            [M6_1,I6_1] = min((th_last(6)-the6).^2);
            th6 = the6(I6_1);
        else
            the5(1) = t5;
            the5(2) = -t5;
            [M5_1,I5_1] = min((th_last(5)-the5).^2);
            th5 = the5(I5_1);
            
            P = [2 0 -1;0 2 1;-1 1 0]\[2*th_last(4);2*th_last(6);atan2(R36(2,1),-R36(1,1))];
            th4 =P(1);
            th6 =P(2);
        end
    else
        th5 = t5;
        
        th4 = atan2((-R36(2,3)/sin(th5)),(-R36(1,3)/sin(th5)));
        th6 = atan2((-R36(3,2)/sin(th5)),(R36(3,1)/sin(th5)));
    end
    
    if isreal(th1) == 1 && ...
       isreal(th2) == 1 && ...
       isreal(th3) == 1 && ...
       isreal(th4) == 1 && ...
       isreal(th5) == 1 && ...
       isreal(th6) == 1 && ...
       count == 0
        reachable = 1;
    else 
        reachable = 0;
    end
    
end