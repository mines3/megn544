clear; clc; close all;

%% Set Simulation Parameters
enable_control = true;
drawCSM = true;  % Draws CSM on the plot, requires a points3D.mat file to exist
Velocity_Mode = true; % Enables theta_dot only based control
Sim_Exact = false; % sets if the simulated geometry is exact (true) or slightly different (false)
simTime = 20; %Run the simulation for the specified number of seconds
makeMovie = false;
uses_geometry = true;  %if true it makes a link-list for use in your functions

%% Define Theta space Trajectory
% [ time_end, th1->th6;...]
load Trajectory_File.mat;
run Geometry.m
trajectory=zeros(max(size(transform_list))+6,8);
for i=1:max(size(transform_list))
    trans=transform_list{i};
    trajectory(i+3,2:4)=points3D(i,:)';
    Q = rot2Quat(trans(1:3,1:3));
    if dot(trajectory(i+2,5:8), Q) < 0
        Q = -Q;
    end
    Q = Q/norm(Q);
    trajectory(i+3,5:8)=Q;
end

% Starting config -- start
paramList = [0 pi/2 0 0 0 0];
H = dhFwdKine(linkList, paramList);
Quat = rot2Quat(H(1:3,1:3));
Quat = Quat/norm(Quat);

if dot(trajectory(3,5:8), Quat) < 0
    Quat = -Quat;
end
trajectory(1,2:4) = H(1:3, 4);
trajectory(2,2:4) = H(1:3, 4);
trajectory(1,5:8) = Quat;
trajectory(2,5:8) = Quat;

% zero angle config -- end
paramList = [0 0 0 0 0 0];
H = dhFwdKine(linkList, paramList);
Quat = rot2Quat(H(1:3,1:3));
Quat = Quat/norm(Quat);

if dot(trajectory(end-1,5:8), Quat) < 0
    Quat = -Quat;
end

trajectory(end,2:4)   = H(1:3, 4);
trajectory(end-1,2:4) = H(1:3, 4);
trajectory(end-2,:) = trajectory(end-3,:);
trajectory(end,5:8) = Quat;
trajectory(end-1,5:8) = Quat;

% Duplicate position 4 into 3 -- start of C
trajectory(3, :) = trajectory(4,:);

for j=2:max(size(transform_list))
    if dot(trajectory(j,5:8),trajectory(j-1,5:8))<0
        trajectory(j,5:8)=-trajectory(j,5:8);
    end
end
t=linspace(3,simTime,max(size(transform_list))+4)';
trajectory(1, 1) = 0;
trajectory(2, 1) = 0.2500;
trajectory(3:end,1)=t;
%% Load ABB Arm Geometry
if uses_geometry
    % assert(exist('velocityJacobian.m','file')==2,'Simulation Error:  Need to add project files to path');
    % assert(exist('transError.m','file')==2,'Simulation Error:  Need to add project files to path');
    % assert(exist('cpMap.m','file')==2,'Simulation Error:  Need to add project files to path');
    % assert(exist('newtonEuler.m','file')==2,'Simulation Error:  Need to add project files to path');
    % assert(exist('dhFwdKine.m','file')==2,'Simulation Error:  Need to add project files to path');
    assert(exist('constAccelInterp.m','file')==2,'Simulation Error:  Need to add project files to path');
    assert(exist('createLink.m','file')==2,'Simulation Error:  Need to add project files to path');
    run('Geometry.m'); 
end

if drawCSM 
    assert(exist('points3D.mat','file')==2,'Simulation Error: Need to make sure a file points3D.mat that has the transformed 2D points in it is on the path');
end 

if Velocity_Mode
    assert(exist('velocityJacobian.m','file')==2,'Simulation Error:  Need to add project files to path');
    assert(exist('transError.m','file')==2,'Simulation Error:  Need to add project files to path');
    assert(exist('cpMap.m','file')==2,'Simulation Error:  Need to add project files to path');
    assert(exist('dhFwdKine.m','file')==2,'Simulation Error:  Need to add project files to path');
end

%% Controller Parameter Definitions
Sim_Name = 'System_Project4';
%Sim_Name = 'System_Theta_Space';
%run Geometry.m; % creates a linkList in the workspace for you and for the simulation
if drawCSM
    load points3D; % loads the CSM trajectory points
    sizeCSM = size(points3D,1);
else
    sizeCSM = 0;
end
open([Sim_Name '.slx']);



%% Define Kp and Kd gains
% If yoru going to tweak, make sure you save these initial values.
Kp = [500;500;500;300;10;10];
Kd = [75;75;75;40;1;1];
Ki = [0;500;300;300;0;0];
K = [1;1;1;1;1;1];


%% Get num points for simulink
traj_length = size(trajectory,1);

%% Choose Simulation System (perfect model or realistic model)
set_param([Sim_Name '/Theta Controller/ABB Arm Dynamics/sim_exact'], 'sw', int2str(Sim_Exact))

%% Enable/Disable Velocity Mode (Ignores Desired Theta Values)
set_param([Sim_Name '/Theta Controller/Velocity_Mode'], 'sw', int2str(~Velocity_Mode))


%% Enable/Disable Control
set_param([Sim_Name '/Theta Controller/control_enable'], 'sw', int2str(enable_control))


%% Run Simulation
simOut =  sim(Sim_Name,'SimulationMode','normal','AbsTol','1e-5','StopTime', int2str(simTime),...
    'SaveState','on','StateSaveName','xout',...
    'SaveOutput','on','OutputSaveName','yout',...
    'SaveFormat', 'array');

%% Extract Variables From Simulation
laser_tracking = simOut.get('laser_tracking');
theta_dot_actual = simOut.get('theta_actual');
theta_actual = simOut.get('theta_actual');
control_torque = simOut.get('control_torque');

%% Plot theta as a function of time
figure(1)
for i=1:6
    subplot(3,2,i)
    plot(theta_actual.time,theta_actual.signals.values(:,i))
    title(['\theta_', int2str(i)])
    xlabel('time (s)')
    ylabel('angle (rad)')
    grid on;
end

%% Display Arm Motion Movie

if makeMovie
    obj = VideoWriter('arm_motion_project4','MPEG-4');
    obj.FrameRate = 30;
    obj.Quality = 50;
    obj.open();
end

figure(2)
plot(0,0); ah = gca; % just need a current axis handel
fh = gcf;
stepSize = fix((1/30)/theta_actual.time(2)); % 30 frames per second
for i=1:stepSize:length(theta_actual.time)
    plotArm(theta_actual.signals.values(i,:),Sim_Exact,ah);
    hold on;
    plot3(reshape(laser_tracking.signals.values(1,4,1:i),[i,1]),... % x pos
        reshape(laser_tracking.signals.values(2,4,1:i),[i,1]),... % y pos
        reshape(laser_tracking.signals.values(3,4,1:i),[i,1]),'r','Parent',ah); % z pos
    if drawCSM
        plot3(points3D(:,1),points3D(:,2),points3D(:,3),'m','Parent',ah);
    end
    hold off;
    if makeMovie
        obj.writeVideo(getframe(fh));
    end
    pause(1/30)
end
if makeMovie
    obj.close();
end

