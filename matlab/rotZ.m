%ROTZ
%   Input: 
%       theta - Scalar angle in radians
%   Output: 
%       zRotationMatrix - Z Rotation Matrix
%   Dependent Methods: None
%   Description:
%       Function that receives an angle and outputs the Z rotation
%       matrix
%
% Jonny Wong
% CWID: 10884664
% MEGN544
% Sept 22, 2021
%
function [R] = rotZ(theta)

R=[cos(theta) -sin(theta) 0;
   sin(theta) cos(theta) 0;
   0 0 1];

end
