%ROT2QUAT
%   Input: 
%       R - 3x3 Rotation Matrix
%   Output: 
%       Q - 4x1 Quaternion Matrix
%   Dependent Methods: None
%   Description:
%       Function that receives a 3x3 rotation matrix and returns a 4x1
%       quaternion
%
% Jonny Wong
% CWID: 10884664
% MEGN544
% Sept 22, 2021
%
function [Q] = rot2Quat(R)

q_0sq=(1+R(1,1)+R(2,2)+R(3,3))/4;
q_1sq=(1+R(1,1)-R(2,2)-R(3,3))/4;
q_2sq=(1-R(1,1)+R(2,2)-R(3,3))/4;
q_3sq=(1-R(1,1)-R(2,2)+R(3,3))/4;
if q_0sq > (q_1sq & q_2sq & q_3sq) 
    q_0=sqrt(q_0sq);
    q_1=((R(3,2)-R(2,3))/(4*q_0));    
    q_2=((R(1,3)-R(3,1))/(4*q_0));
    q_3=((R(2,1)-R(1,2))/(4*q_0));
    Q=[q_0; q_1; q_2; q_3];
elseif q_1sq > (q_0sq & q_2sq & q_3sq)
    q_1=sqrt(q_1sq);
    q_0=((R(3,2)-R(2,3))/(4*q_1)); 
    q_2=((R(1,2)+R(2,1))/(4*q_1));
    q_3=((R(1,3)+R(3,1))/(4*q_1));
    Q=[q_0; q_1; q_2; q_3];
elseif q_2sq > (q_0sq & q_1sq & q_3sq)
    q_2=sqrt(q_2sq);
    q_0=((R(1,3)-R(3,1))/(4*q_2));
    q_1=((R(1,2)+R(2,1))/(4*q_2));
    q_3=((R(2,3)+R(3,2))/(4*q_2));
    Q=[q_0; q_1; q_2; q_3];
else 
    q_3=sqrt(q_3sq);
    q_0=((R(2,1)-R(1,2))/(4*q_3));
    q_1=((R(1,3)+R(3,1))/(4*q_3));
    q_2=((R(2,3)+R(3,2))/(4*q_3));
    Q=[q_0; q_1; q_2; q_3];
end

end

