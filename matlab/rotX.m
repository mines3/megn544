%ROTX
%   Input: 
%       theta - scalar angle in radians
%   Output: 
%       xRotationMatrix - X Rotation Matrix
%   Dependent Methods: None
%   Description:
%       Function that receives an angle and outputs the X rotation
%       matrix
%
% Jonny Wong
% CWID: 10884664
% MEGN544
% Sept 22, 2021
%
function [R] = rotX(theta)

R=[1 0 0;
   0 cos(theta) -sin(theta);
   0 sin(theta) cos(theta)];

end

