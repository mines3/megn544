%DHINVKINE 
%   Input: 
%       linkList - A list of link parameters for the robot arm manipulator
%       desTransform - Homogeneous Transformation matrix of final
%                      destination.
%       paramListGuess - A list of guessed theta values
%   Output: 
%       paramList - A list of theta values or point values depending on
%                   if in theta space or cartesian space.
%       error - A percent error value
%   Dependent Methods: dhFwdKine, velocityJacobian
%   Description:
%       A function that generates a new list of thetas or points using the
%       DH inverse kinematics approach to solve for a 6-Arm manipulator.
%       The function also returns the percent error from the current
%       transformation matrix to the final destination.
%
%
%
%
%
%
%
% Jonny Wong
% CWID: 10884664
% MEGN544
% Nov 14, 2021
%
function [paramList, error] = dhInvKine (linkList, desTransform, paramListGuess)
    linkSize = max(size(linkList));
    delt_q = ones(linkSize,1);
    error = 1;
    maxAttempts = 0;
    tol = 1e-9;

    if ~exist('paramListGuess', 'var')
        paramListGuess = zeros(size(linkList), 1);
    end

    while (norm(delt_q) > tol) && (norm(error) > tol) && (maxAttempts < 100)
        T_c = dhFwdKine(linkList, paramListGuess);
        error = transError(desTransform, T_c);

        Jv = velocityJacobian(linkList, paramListGuess);
        [U, S, V] = svd(Jv);
        ES = S';
        for i = 1:min(size(S))
            if (S(1,1)/S(i, i)) > 500
                ES(i,i) = 0;
            else
                ES(i,i) = 1/ES(i,i);
            end
        end
        JvInv = V*ES*U';
        
        delt_q = JvInv*error;
        paramListGuess = paramListGuess + delt_q;
        
        error = max(abs(error));
        maxAttempts = maxAttempts + 1;
    end
    paramList = paramListGuess;
end

