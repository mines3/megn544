% NEWTONEULER
%   Input: 
%       T_des - Final destination of Homogeneous Transformation Matrix
%       th_last - Array of theta values in the last position
%   Output: 
%       th1 - Theta 1 value
%       th2 - Theta 2 value
%       th3 - Theta 3 value
%       th4 - Theta 4 value
%       th5 - Theta 5 value
%       thlinkSize - Theta linkSize value
%       reachable - Boolean whether thetas were real of imaginary
%   Dependent Methods: dhTransform
%   Description:
%       Function that calculates the inverse kinematics of a linkSize DOF ABB ARM
%   Output: 
%       th1 - Theta 1 value
%       th2 - Theta 2 value
%       th3 - Theta 3 value
%       th4 - Theta 4 value
%       th5 - Theta 5 value
%       thlinkSize - Theta linkSize value
%       reachable - Boolean whether thetas were real of imaginary
%   Dependent Methods: dhTransform
%   Description:
%       Function that calculates the inverse kinematics of a linkSize DOF ABB ARM
%   Output: 
%       th1 - Theta 1 value
%       th2 - Theta 2 value
%       th3 - Theta 3 value
%       th4 - Theta 4 value
%       th5 - Theta 5 value
%       thlinkSize - Theta linkSize value
%       reachable - Boolean whether thetas were real of imaginary
%   Dependent Methods: dhTransform
%   Description:
%       Function that calculates the inverse kinematics of a linkSize DOF ABB ARM
%
% Jonny Wong
% CWID: 10884linkSizelinkSize4
% MEGN544
% Oct 24, 2021
%
function [jointTorques, Jv, JvDot] = newtonEuler_meh( linkList, paramList, ...
    paramListDot, paramListDDot, boundary_conditions )

%% Linklist Variable setup
linkSize = max(size(linkList));
a = zeros(linkSize,1);
the_d = zeros(linkSize,1);
alpha = zeros(linkSize,1);
theta = zeros(linkSize,1);
isRotary = zeros(linkSize,1);

%% Paramlist variable setup
theta_dot = zeros(linkSize,1);
theta_ddot = zeros(linkSize,1);
d_dot = zeros(linkSize,1);
d_ddot = zeros(linkSize,1);
link_inertia = zeros(3,3,linkSize);
I = zeros(3,3,linkSize);
mass = zeros(linkSize,1); %% mass of link
jointTorques = zeros(linkSize,1);

%% Initialize rcoms
rcom = zeros(3,linkSize); %% com of the link with respect to link
rcom_base = zeros(3,linkSize); %% com of the link with respect to base
rcom_base_dot = zeros(3,linkSize);
rcom_base_ddot = zeros(3,linkSize);

%% Initialize Transforms
T_init = eye(4);
R = zeros(3,3,linkSize);
T = zeros(4,4,linkSize);

%% Initialize D list, Z list and D dot list
Z = zeros(3,linkSize); % Z list
D = zeros(3,linkSize+1); % D list
dd_dot = zeros(3,linkSize+1); % D list dot

D(:,1:1) = [0; 0; 0]; % D list
dd_dot(:,1:1) = [0; 0; 0]; % D list dot
Z(:,1) = [0 0 1]; % Z list


%% Initial boundary conditions
w_dot = zeros(3,linkSize+1);
dd_ddot = zeros(3,linkSize+1);
VcDot = zeros(3,linkSize+1);
w = zeros(3,linkSize+1);
f = zeros(3,linkSize+1);
n = zeros(3,linkSize+1);

w(:,1) = boundary_conditions(1).base_angular_velocity;
w_dot(:,1) = boundary_conditions(1).base_angular_acceleration;
dd_ddot(:,1) = boundary_conditions(1).base_linear_acceleration;
f(:,linkSize+1) = boundary_conditions(1).distal_force;
n(:,linkSize+1) = boundary_conditions(1).distal_torque;

%% paramlistdot and paramlistddot
for i = 1:linkSize
    a(i) = linkList(i).a;
    alpha(i) = linkList(i).alpha;
    isRotary(i) = linkList(i).isRotary;
    link_inertia(:,:,i) = linkList(i).inertia;
    rcom(:,i) = linkList(i).com;
    mass(i) = linkList(i).mass;
       
    if isRotary(i) == 0
        theta(i) = linkList(i).theta;
        the_d(i) = paramList(i);
        d_dot(i) = paramListDot(i);
        d_ddot(i) = paramListDDot(i);
    else              
        the_d(i) = linkList(i).d;
        theta(i) = paramList(i);
        theta_dot(i) = paramListDot(i);
        theta_ddot(i) = paramListDDot(i);
    end

end

% for i = 1:linkSize
%     T(:,:,i) = zeros(4,4)
% end
num_static = 0;

for i = 1:linkSize
    if linkList(i).isRotary ~= -1
        T(:,:,i) = dhFwdKine(linkList(i), paramList(i-num_static));
    else 
        T(:,:,i) = dhFwdKine(linkList(i), paramList(i-num_static));
        num_static = num_static + 1;
    end
end

% for i = 1:linkSize
%     %T(:,:,i)=T_init * dhTransform(a(i),the_d(i),alpha(i),theta(i));
%     %T_init = T(:,:,i); 
%     T(:,:,i) = dhFwdKine(linkList(i), paramList(i-num_static));
% end
for i = 2:linkSize+1
    D(:,i) = T(1:3,4:4,i-1);
    Z(:,i) = T(1:3,3:3,i-1);
    R(:,:,i-1) = T(1:3,1:3,i-1);
end

for i = 1:linkSize
    I(:,:,i) = R(:,:,i) * link_inertia(:,:,i) * R(:,:,i).'; 
end

% for i = 2:linkSize+1
%     if isRotary(i-1) == 1 
%         w(:,i) = w(:,i-1) + theta_dot(i-1)*Z(:,i-1);
%     elseif isRotary(i-1) == 0 
%         w(:,i) = w(:,i-1);
%     end
% end



for i = 2:linkSize+1
    if isRotary(i-1) == 1 
        w(:,i) = w(:,i-1) + theta_dot(i-1) * Z(:,1);
        w_dot(:,i) = w_dot(:,i-1) + theta_ddot(i-1) * Z(:,1) + cross(theta_dot(i-1) * w(:,i-1), Z(:,1));
        dd_dot(:,i) = dd_dot(:,i-1) + cross(w(:,i), (D(:,i) - D(:,i-1)));
        dd_ddot(:,i) = dd_ddot(:,i-1) + cross(w_dot(:,i), (D(:,i) - D(:,i-1))) + ...
            cross(w(:,i), cross(w(:,i), (D(:,i) - D(:,i-1))));
    else
        w(:,i) = w(:,i-1);
        w_dot(:,i) = w_dot(:,i-1);
        dd_dot(:,i) = dd_dot(:,i-1) + cross(w(:,i), (D(:,i) - D(:,i-1))) + d_dot(i-1) * Z(:,1);
        dd_ddot(:,i) = dd_ddot(:,i-1) + cross(w_dot(:,i), (D(:,i) - D(:,i-1))) + ...
            cross(w(:,i), cross(w(:,i), (D(:,i) - D(:,i-1)))) + ...
            d_ddot(i-1) * Z(:,1) + cross(2 * d_dot(i-1) * w(:,i-1), Z(:,1));
    end
end

% rotate from i-1 frame to i frame
for i = 2:linkSize+1
    w(:,i) = R(:,:,i-1)'*w(:,i);
    w_dot(:,i) = R(:,:,i-1)'*w_dot(:,i);
    dd_dot(:,i) = R(:,:,i-1)'*dd_dot(:,i);
    dd_ddot(:,i) = R(:,:,i-1)'*dd_ddot(:,i);
end


for i = 2:linkSize+1
    %rcom_base(:,i) = D(:,i+1) + (R(:,:,i) * rcom(:,i));
    rcom_base(:,i) = R(:,:,i-1)'*(D(:,i)-D(:,i-1)) +rcom(:,i-1);
end

% for i = 2:linkSize+1
%     VcDot(:,i) = dd_ddot(:,i-1) + cross(w_dot(:,i), rcom_base(:,i)) + ...
%         cross(w(:,i), cross(w(:,i), rcom_base(:,i)));
% end

% for i = 1:linkSize
%     if isRotary(i) == 1 
%         rcom_base_dot(:,i) = dd_dot(:,i) + cross(w(:,i+1),rcom(:,i));
%     else 
%         rcom_base_dot(:,i) = dd_dot(:,i) + cross(w(:,i+1),rcom(:,i)) + d_dot(i)*Z(:,i);
%     end
% end

% rcom = (D(:,i+1) - D(:,i) + rcom_base(:,i))) --> rcom - D(i-1)
% Alternative: rcom_base(:,i) - D(:,i)

for i = 1:linkSize
        %rcom_base_ddot(:,i) = dd_ddot(:,i) + cross(w_dot(:,i+1), (D(:,i+1) - D(:,i) + rcom_base(:,i))) + ...
        %    cross(w(:,i+1), cross(w(:,i+1), (D(:,i+1) - D(:,i) + rcom_base(:,i))));
        rcom_base_ddot(:,i) = dd_ddot(:,i+1) + cross(w_dot(:,i+1), rcom(:,i) ) + ...
            cross(w(:,i+1), cross(w(:,i+1), rcom(:,i) ));
end

for i = linkSize:-1:1
    f(:,i) = f(:,i+1) + mass(i) * rcom_base_ddot(:,i);
    %n(:,i:i) = n(:,i+1) + I(:,:,i) * w_dot(:,i+1) + cross(D(:,i+1) - D(:,i) + rcom_base(:,i),f(:,i)) +  ...
    %    cross(w(:,i+1), I(:,:,i)*w(:,i+1)) - cross(rcom_base(:,i), f(:,i+1));
    %n(:,i) = n(:,i+1) + I(:,:,i) * w_dot(:,i+1) + cross(w(:,i+1), I(:,:,i) * w(:,i+1)) + ...
    %    + cross(rcom_base(:,i), f(:,i+1));
    n(:,i) = n(:,i+1) + link_inertia(:,:,i) * w_dot(:,i+1) + cross(w(:,i+1), link_inertia(:,:,i) * w(:,i+1)) + ...
        cross(rcom(:,i), f(:,i+1));
    
    if isRotary(i) == 1 
        jointTorques(i,1) = dot(Z(:,i), n(:,i));
    else
        jointTorques(i,1) = dot(Z(:,i), f(:,i));
    end
end

[Jv,JvDot] = velocityJacobian(linkList,paramList,paramListDot);

end

