function T = WinXFwd(th_list,d1,a2,a3,d5)

alpha1 = -pi/2;
alpha2 = pi;
alpha4 = -pi/2;

t1 = th_list(1);
t2 = th_list(2);
t3 = th_list(3);
t4 = th_list(4);
t5 = th_list(5);


T = dhTransform(0,d1,alpha1,t1)*dhTransform(a2,0,alpha2,t2)*...
    dhTransform(a3,0,0,t3)*dhTransform(0,0,alpha4,t4)*dhTransform(0,d5,0,t5);

end