clear;clc;
A = [0 1;0 0];
B = [0;1];
%open_loop_poles = eig(A)

%Ar = A;
%Ar(3,:) = Ar(3,:)+2*(rand(1,3)-.5).*Ar(3,:)*.1


%% Second Order Step Response
% f = @(sig,z,t) 1 - 1/(sqrt(1-z^2)).*exp(-sig*t).*sin(sig*sqrt(1-z^2)/z*t + acos(z));
% time = linspace(0,1,100);
% figure(1)
% plot(time,f(10,.707,time))
% %%
% z = linspace(.01,.99,100);
% s = zeros(size(z));
% for i = 1:100, s(i) = fminsearch(@(x) (f(x,z(i),.01)-.9)^2,1.8*z(i)/.01)*.01; end
% figure(2)
% plot(z,s)
% %%
% lsq_soln = [ z' z'.^3]\s'
% figure(2)
% plot(z,s,'k',z,[ z' z'.^3]*lsq_soln,'r')

%%
% Ts = 1.5 sec; Tr = .75 sec;
Tr = .75;
Ts = 1.5;
os = 0.025; 

% Desired max os is 5% plan on 2% for fs
zeta = sqrt(log(os)^2/(pi^2+log(os)^2));
sigma = max(4.6/(Ts*.95),(1.53*zeta + 2.31*zeta^3)/(Tr*.95));

poles = -sigma*([1;1] + [sqrt(1-zeta^2)/zeta*1i;-sqrt(1-zeta^2)/zeta *1i]);

K = place(A,B,poles);

Bss = (B'*B)^-1*B'*A;


%%  Bessel Filter Placement
Tp = 1; % desired peak time
[~,poles_b,~] = besself(size(A,1),2*pi/Tp)
K = place(A,B,poles_b)


%% zoh sys
dt = .01;
tmp = [A B;0 0 0];
tmp_d = expm(tmp*dt)

Ad = tmp_d(1:2,1:2);
Bd = tmp_d(1:2,3);

Pd = exp(poles*dt)

Kd = place(Ad,Bd,Pd)

%% Bessel Filter Placement
% Pd = exp(poles_b*dt)
% Kd = place(Ad,Bd,Pd)

%% Design Observer
% C = [1 0 0];
% 
% obs_p =  poles*5
% obs_p_d = exp(obs_p*dt)
% 
% Ltrans = place(Ad',C',obs_p_d);
% L = Ltrans'
% 
% 
% 
% %%  Bessel Filter Placement
% [~,poles_b,~] = besself(size(A,1),2*pi/Tp )
% 
% obs_poles_descrete = exp(poles_b*dt);
% Ltrans = place(Ad',C',obs_poles_descrete);
% L = Ltrans'
