%ANGLEAXIS2ROT 
%   Input:
%       k - Matrix (3x1)
%       theta - scalar value in radians
%   Output:
%       R - 3x3 Rotation Matrix
%   Dependent Methods: cpMap
%   Description:
%       Function that receives a k_hat 3x1 matrix and a theta angle and 
%       returns a 3x3 rotation matrix
%
% Jonny Wong
% CWID: 10884664
% MEGN544
% Sept 22, 2021
%
function [R] = angleAxis2Rot(k,theta)

R = cos(theta) * eye(3) + sin(theta)*cpMap(k) + (1-cos(theta))*(k*k');

end

