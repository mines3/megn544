function plot_points3D_scaled()
close all

% Scaling factor = 0.02
% X needs to shift by -.06
% Z needs to shift by 0.4

load('points2D.mat');


plot((points_all(:,1)*.02) - .06, (points_all(:,2)*.02) + 0.4)
axis([-1 max(points_all(:,1))+1 -1 max(points_all(:,2))+1])

Ux = zeros(1,22);
Vx = zeros(1,22);
Wx = zeros(1,22);
Uy = zeros(1,22);
Vy = zeros(1,22);
Wy = zeros(1,22);
Uz = zeros(1,22);
Vz = zeros(1,22);
Wz = zeros(1,22);

points3D = zeros(22,3);

points3D(:,1) = (points_all(:,1)*.02) - .06;
points3D(:,2) = -0.3;
points3D(:,3) = (points_all(:,2)*.02) + 0.4;

figure(1);
plot3(points3D(:,1), points3D(:,2), points3D(:,3));
xlabel('X');
ylabel('Y');
zlabel('Z');

theta = zeros(22,1);
theta(1,1) = 0;

for i = 2:22
    theta(i,1) = -atan2((points3D(i,3) - points3D(i-1,3)), (points3D(i,1) - points3D(i-1,1)));
    %theta(i,1) = atan2((points_new(i,1) - points_new(i-1,1)), (points_new(i,3) - points_new(i-1,3)));
end

T = eye(4);
[th1, th2, th3, th4, th5, th6, reachable] = deal(zeros(24,1));

th2(end) = pi/2;
th2(end-1) = pi/2;
run('Geometry.m'); 

for i = 1:21
    %R = rotX(pi/2) * rotZ(theta(i+1,1));
    R = rotY(theta(i+1,1)) * rotX(pi/2);
    Ux(i) = R(1,1);
    Vx(i) = R(2,1);
    Wx(i) = R(3,1);
    
    Uy(i) = R(1,2);
    Vy(i) = R(2,2);
    Wy(i) = R(3,2);
    
    Uz(i) = R(1,3);
    Vz(i) = R(2,3);
    Wz(i) = R(3,3);
    T(1:3, 1:3) = R;
    T(1:3, 4) = points3D(i, 1:3)'
    if i == 1
        [th1(i), th2(i), th3(i), th4(i), th5(i), th6(i), reachable(i)] = abbInvKine(T, [0, 0, 0, 0, 0, 0]);
    else
        [th1(i), th2(i), th3(i), th4(i), th5(i), th6(i), reachable(i)] = abbInvKine(T, [th1(i-1), th2(i-1), th3(i-1), th4(i-1), th5(i-1), th6(i-1)]);
    end
    if i == 21
        Ux(22) = R(1,1);
        Vx(22) = R(2,1);
        Wx(22) = R(3,1);

        Uy(22) = R(1,2);
        Vy(22) = R(2,2);
        Wy(22) = R(3,2);

        Uz(22) = R(1,3);
        Vz(22) = R(2,3);
        Wz(22) = R(3,3);
        T(1:3, 4) = [0.14; -0.30; 0.40];
        [th1(22), th2(22), th3(22), th4(22), th5(22), th6(22), reachable(22)] = abbInvKine(T, [th1(i), th2(i), th3(i), th4(i), th5(i), th6(i)]);
    end
    
end
%T_final = eye(4);
%T_final(1:3, 1:3) = T(1:3, 1:3);
%T_final(1:3, 4) = [0.14; -0.30; 0.40];
%[th1(23), th2(23), th3(23), th4(23), th5(23), th6(23), reachable(23)] = abbInvKine(T_final, [th1(22), th2(22), th3(22), th4(22), th5(22), th6(22)]);


thetasY = [th1 th2 th3 th4 th5 th6 reachable];
save('thetasY', 'thetasY');

figure(2);
quiver3(points3D(:,1)', points3D(:,2)', points3D(:,3)', Uz, Vz, Wz, .5, 'Blue');
hold on;
quiver3(points3D(:,1)', points3D(:,2)', points3D(:,3)', Ux, Vx, Wx, .5, 'Red');
quiver3(points3D(:,1)', points3D(:,2)', points3D(:,3)', Uy, Vy, Wy, .5, 'Green');
hold off;
xlabel('X');
ylabel('Y');
zlabel('Z');
legend('Z', 'X', 'Y')
axis equal 

figure(3)
plot(points_C(:,1),points_C(:,2),'r',...
     points_S(:,1),points_S(:,2),'g',...
     points_M(:,1),points_M(:,2),'b')
axis([-1 max(points_all(:,1))+1 -1 max(points_all(:,2))+1])

save('points2D','points_C','points_S','points_M','points_all')
end