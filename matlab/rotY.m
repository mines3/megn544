%ROTY
%   Input: 
%       theta - Scalar angle in radians
%   Output: 
%       yRotationMatrix - Y Rotation Matrix
%   Dependent Methods: None
%   Description:
%       Function that receives an angle and outputs the Y rotation
%       matrix
%
% Jonny Wong
% CWID: 10884664
% MEGN544
% Sept 22, 2021
%
function [yRotationMatrix] = rotY(theta)

yRotationMatrix =...
    [cos(theta) 0 sin(theta);  0 1 0; -sin(theta) 0 cos(theta)];

end

