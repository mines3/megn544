%DHFWDKINE 
%   Input: 
%       linkList - Array of structs containing a, d, alpha, theta, offset,
%                  mass, inertia, center of mass & isRotary
%       paramList - an array containing the current state of their 
%                   joint variables, according to the robot’s encoders.
%   Output: 
%       H - Homogeneous Transformation Matrix of a dhTransform
%   Dependent Methods: dhTransform
%   Description:
%       Function that calculates the homogeneous tranformation matrix
%
% Jonny Wong
% CWID: 10884664
% MEGN544
% Oct 24, 2021
%
function [H] = dhFwdKine(linkList, paramList)

    H = eye(4);
    for i = 1:max(size(linkList))
        if linkList(i).isRotary==1
            linkList(i).theta=paramList(i)-linkList(i).offset;
        elseif linkList(i).isRotary==0
            linkList(i).d=paramList(i)-linkList(i).offset;
        end
        H = H * dhTransform(linkList(i).a, linkList(i).d, linkList(i).alpha, linkList(i).theta);
    end

end