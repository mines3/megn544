%VELOCITYJACOBIAN 
%   Input: 
%       linkList - A list of link parameters for the robot arm manipulator
%       
%       paramList - A list of guessed theta values
%
%       paramRateList - A list of d_dots and theta_dots that are used if
%                       the values exist.
%   Output: 
%       Jv - The 6 x LinkSize velocity jacobian
%       JvDot - The 6 x LinkSize derivative of the velocity jacobian
%   Dependent Methods: dhTransform
%   Description:
%       Function that reads in the size of the links through a structure, 
%       a list of parameter values used for d's or thetas if the link is 
%       a rotary or not. A list of d and theta derivative values
%
%
%
%
%
%
% Jonny Wong
% CWID: 10884664
% MEGN544
% Nov 14, 2021
%
function [Jv, JvDot] = velocityJacobian(linkList, paramList, paramRateList)
    linkSize = max(size(linkList));
    theta_dot = zeros(linkSize,1);
    d_dot = zeros(linkSize,1);
    T = eye(4);
    JvDot = zeros(6, linkSize);
    Jv = zeros(6, linkSize);
    Z = zeros(3, linkSize + 1);
    D = zeros(3, linkSize + 1);
    W = zeros(3, linkSize + 1);
    dd_dot = zeros(3,linkSize + 1);
    
    for i = 1:linkSize
        Z(:, i) = T(1:3, 3);
        D(:, i) = T(1:3, 4);
        if linkList(i).isRotary == 0
            T = T * dhTransform(linkList(i).a, paramList(i) - linkList(i).offset, linkList(i).alpha, linkList(i).theta);
            
            if exist('paramRateList','var')
                d_dot(i) = paramRateList(i);
            end
        else        
            
            T = T * dhTransform(linkList(i).a, linkList(i).d, linkList(i).alpha, paramList(i) - linkList(i).offset);
            
            if exist('paramRateList','var')
                theta_dot(i) = paramRateList(i);
            end
        end
    end
    Z(:, end) = T(1:3, 3);
    D(:, end) = T(1:3, 4);
    
    for i = 1:linkSize
        if linkList(i).isRotary == 0 
            Jv(1:3, i) = Z(:,i);
            Jv(4:6, i) = [0;0;0];
        else
            Jv(1:3, i) = cross(Z(:,i), (D(:, end) - D(:, i)));
            Jv(4:6, i) = Z(:,i);
        end
        
    end
    
    W(:, 1) = [0; 0; 0];
    dd_dot(:,1) = [0; 0; 0];

    for i = 2:linkSize+1
        if linkList(i-1).isRotary == 0 
            W(:, i) = W(:, i-1);
            dd_dot(:,i) = dd_dot(:,i-1) + cross(W(:,i),(D(:,i)-D(:,i-1))) + d_dot(i-1)*Z(:,i-1);
        else
            W(:, i) = theta_dot(i-1) * Z(:, i-1) + W(:, i-1);
            dd_dot(:,i) = dd_dot(:,i-1) + cross(W(:,i),(D(:,i)-D(:,i-1)));
        end
    end
    
    
        
    if exist('paramRateList','var')
        for i = 1:linkSize
            if linkList(i).isRotary == 1
                JvDot(:,i) = [cross(cross(W(:,i),Z(:,i)),(D(:,end) - D(:,i))) ...
                    + cross(Z(:,i), dd_dot(:,end) - dd_dot(:,i)); cross(W(:,i),Z(:,i))];
            else 
                JvDot(:,i) = [cross(W(:,i),Z(:,i)); 0; 0; 0];
            end
        end
    else
        JvDot = []; % empty value for JvDot if no third input value
    end
end