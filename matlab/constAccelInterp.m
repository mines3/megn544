%CONSTACCELINTERP 
%   Input: 
%       t - time now
%       trajectory - List of time points
%       transPercent - Percentage of time for tau
%   Output: 
%       p - Position of link
%       v - Velocity of link
%       a - Acceleration of link
%   Dependent Methods: None
%   Description:
%       Function that calculates the constant acceleration and 
%       interpolation and returns the position, velocity and acceleration
%
%
%
%
% Jonny Wong
% CWID: 10884664
% MEGN544
% Oct 24, 2021
%
function [p,v,a] = constAccelInterp(t,trajectory,transPercent)


t1=trajectory(:,1);
[m,n]=size(trajectory);
[p,v,a] = deal(zeros(1,n-1));

pos = trajectory(:, 2:n);

for i=1:m-1
    tau(i)=transPercent*(t1(i+1)-t1(i));
end
tau(m)=tau(m-1);

for i = 1:n-1
    for j = 2:m-1
        % Case for during interpolation
        if t <= (t1(j) + tau(j)) && t >= (t1(j) - tau(j))
            del_p1 = pos(j, i) - pos(j-1,i);
            del_p2 = pos(j+1, i) - pos(j,i);
            t2 = t1(j+1) - t1(j);
            t3 = t1(j) - t1(j-1);
            
            p(i) = pos(j, i) - (((t - t1(j) - tau(j))^2) * ...
                del_p1/(4 * tau(j) * (t1(j) - t1(j-1)))) + ...
                (((t - t1(j) + tau(j))^2) * del_p2/(4 * tau(j) * t2));
            
            v(i) = ((del_p2 * (t - t1(j) + tau(j)))/(2 * tau(j) * t2)) - ...
                ((del_p1 * (t - t1(j) - tau(j)))/(2 * tau(j) * t3));
            
            a(i) = ((del_p2)/(2 * tau(j) * (t1(j+1) - t1(j)))) - ((del_p1)/(2 * tau(j)*t3));
        % Case for Straight line no interpolation
        elseif t > (t1(j) + tau(j)) && t <= (t1(j+1) - tau(j+1))
            del_p2 = pos(j+1, i) - pos(j, i);
            t2 = t1(j+1) - t1(j);
            
            p(i) = pos(j, i) + (((-t1(j) + t)/t2) * del_p2);
            v(i) = del_p2/t2;
            a(i) = 0;
        % Case for when between time 0 + tau
        elseif j-1 == 1 && t > t1(j-1) && t < (t1(j-1) + tau(j-1))
            del_p2 = pos(j) - pos(j-1);
            t2 = t1(j) - t1(j-1);
            t3 = t1(j+1)-t1(j);
            
            p(i) = pos(j-1,i) + (((t - t1(j-1) + tau(j-1))^2) * del_p2/(4 * tau(j-1)*t2));
            a(i) = 0.5*tau(j) * (del_p2/t3);
            v(i) = 0.5*tau(j) * (del_p2 * (t - t1(j) + tau(j))/t3);
        % Case 1 for when between time 5 - tau
        elseif (j+1) == m && t < t1(j+1) && t > (t1(j+1) - tau(j+1))
            del_p2 = pos(j+1, i) - pos(j+1, i);
            t2 = t1(j+1) - t1(j);
            t3 = t1(j+1) - t1(j);
            
            p(i) = pos(j+1, i) + (((t - t1(j+1) - tau(j+1))^2) * del_p2/(4 * tau(j+1) * (t2)));
            v(i) = 0.5*tau(j+1) * (del_p2 * (t - t1(j+1) - tau(j+1))/(t3));
            a(i) = 0.5*tau(j+1) * (del_p2/t3);
        end
    end
end




