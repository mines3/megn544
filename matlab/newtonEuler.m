% NEWTONEULER
%   Input: 
%       linkList - Structure of the robot arm's geometry
%       paramList - List of thetas/d's
%       paramListDot - List of theta dots and d dots
%       paramListDDot - List of theta double dot and d double dot
%       boundry_conditions - Boundary conditions for base_angular_velocity,
%           base_angular_acceleration, base_linear_acceleration,
%           base_angular_velocity, distal_force, distal_torques
%   Output: 
%       jointTorques - Torques of the joints
%       Jv - Velocity Jacobian
%       JvDot - Derivative of the velocity jacobian
%   Dependent Methods: dhFwdKine, velocityJacobian
%   Description:
%       Function that does the newton euler calculations and outputs
%       the joint torques of the robot arm as well as the velocity jacobian
%
%
%
%
%
%
%
%
%
%
%
%
% Jonny Wong
% CWID: 10884664
% MEGN544
% Dec 07, 2021
%
function [jointTorques, Jv, JvDot] = newtonEuler( linkList, paramList, paramListDot, paramListDDot, boundry_conditions )
numJoints = length(linkList);

% Pre allocate a list of values that need to be stored in memory between
% loops
list = repmat(struct( 'Zlast', zeros(3,1),...   % Z i-1, rotation axis for link i in base frame
    'Woi', zeros(3,1),...     % Angular velocity of origin i in base frame
    'doi', zeros(3,1),...     % Position of Origin i relative to base in base frame
    'doi_dot', zeros(3,1),... % Velocity of Origin i relative to base in base frame
    'Fi', zeros(3,1),...      % Inertial Force on link i in base frame
    'Ni', zeros(3,1),... % Inertial Torque on link i in base frame
    'rii', zeros(3,1),...% Displacement from i-1 to com
    'ri1_i',zeros(3,1) ),...% Displacemenbt from i to com
    numJoints,1);

% Initialize link variables that get propagated forward
Toi = eye(4); % Transform from 0 to joint i
V = zeros(3,1); % Velocity in joint frame
W = boundry_conditions.base_angular_velocity; % Angluar Velocity in joint frame
Wdot = boundry_conditions.base_angular_acceleration; % Angular Acceleration in joint frame
Vdot = boundry_conditions.base_linear_acceleration; % Linear acceleration in joint frame 

W_BC = boundry_conditions.base_angular_velocity; %Necessary for Jv and Jvdot

num_static = 0; % number of static links encountered

for i=1:numJoints % begin forward iteration from base to tool
    
    % Calculate link transform from i-1 to i
    if linkList(i).isRotary ~= -1
        % hint i-num_static is the param index to be on
        Ti = dhFwdKine(linkList(i), paramList(i-num_static));
    else
        Ti = dhFwdKine(linkList(i), paramList(i-num_static));
        num_static = num_static+1;
    end
    
    % extract distance from joint i-1 to joint i
    di = Ti(1:3,4);
    
    % Roi
    Roi = Toi(1:3,1:3);
    
    % extract rotation from joint i to joint i-1 (transpose of i-1 to i
    % rotation)
    Rt = Ti(1:3,1:3)';
    
    % Update joint frame velocity, acceleartion, angular Acceleration, and
    % angular velocity.  In the i-i frame (so z is [0;0;1])
    Z = [0;0;1];
    if linkList(i).isRotary == 1
        Wdot = Wdot + paramListDDot(i-num_static) * Z + cross(paramListDot(i-num_static) * W, Z); % update ang accel in joint frame
        W = W + paramListDot(i-num_static) * Z; % updage ang vel
        
        % V is only used in Jv Dot. Make sure you use W-Roi'*W_BC to remove base
        % acceleration
        V = V + cross(W, di);  % update velocity in joint frame
        Vdot = Vdot + cross(Wdot, di) + cross(W, cross(W, di)); % update accel in joint frame
    elseif linkList(i).isRotary == 0
        Wdot = Wdot; % update ang accel in joint frame
        W = W; % updage ang vel
        
        % V is only used in Jv Dot. Make sure you use W-W_BC to remove base
        % acceleration
        V = V + cross(W, di) + paramListDot(i-num_static) * Z;  % update velocity in joint frame
        Vdot = Vdot + cross(Wdot, di) + cross(W, cross(W, di)) + ...
            paramListDDot(i-num_static) * Z + cross(2 * paramListDot(i-num_static) * W, Z); % update accel in joint frame
    else
        Wdot = Wdot; % update ang accel in joint frame
        W = W; % updage ang vel
        
        % V is only used in Jv Dot. Make sure you use W-W_BC to remove base
        % acceleration
        V = V + cross(W, di);  % update velocity in joint frame
        Vdot = Vdot + cross(Wdot, di) + cross(W, cross(W, di)); % update accel in joint frame
    end
    
    % rotate from i-1 frame to i frame
    Wdot = Rt*Wdot;
    W = Rt*W;
    V = Rt*V;
    Vdot = Rt*Vdot;
    
    % Calculate the displacement from the i-1 frame to the i'th com in
    % the i'th frame
    ri1_i =  Rt*di+linkList(i).com;
    
    % Calculate the Acceleration of the Center of Mass of the link in
    % the ith frame
    Vcdot = Vdot  + cross(Wdot, linkList(i).com ) + cross(W, cross(W, linkList(i).com ));
    
    % Calculate and Save Inertial Force and Torque in the i'th frame
    F = linkList(i).mass * Vcdot; % Newton's Equation
    N = linkList(i).inertia * Wdot + cross(W, linkList(i).inertia * W); % Euler's Equation
    
    
    
    % Save values specific to calculating Jv and JvDot that we already know
    list(i).Zlast  = Toi(1:3,3); % Save Zi-1 for Jv and JvDot
    Toi = Toi*Ti; % Update base to link transform
    list(i).doi = Toi(1:3,4); % save distance from base to joint i for Jv and JvDot
    list(i).Woi = Toi(1:3,1:3)*W; % Save Wi i base frame for JvDot
    list(i).doi_dot = Toi(1:3,1:3)*V; % Save Vi in base frame for JvDot
    list(i).Fi = Toi(1:3,1:3)*F; % Inertial Force in base frame
    list(i).Ni = Toi(1:3,1:3)*N; % Inertial Torque in base frame
    list(i).ri1_i = Toi(1:3,1:3)*ri1_i; % Displacement from i-1 to com
    list(i).rii   = Toi(1:3,1:3)*linkList(i).com; % Displacemenbt from i to com
end % End Forward Iterations

% Initialize variables for calculating Jv and JvDot
Jv = zeros(6,numJoints);    % preallocate Jv  for speed
JvDot = zeros(6,numJoints); % preallocate JvDot for speed
doN = list(end).doi; % Extract Distance from Base to Tool in base frame
voN = list(end).doi_dot; % Extract Velocity of Tool in base frame

% Initialize variables for force/torque propagation
f = boundry_conditions.distal_force; % Initialize force to external force on the tool in the tool frame
n = boundry_conditions.distal_torque; % Initialize torque to external torque on the tool in the tool frame

% Rotate f & n to base frame
f = Toi(1:3, 1:3) *f;
n = Toi(1:3, 1:3) *n;
% preallocate joint torque vector
jointTorques = zeros(numJoints,1); % preallocate for speed

for i = numJoints:-1:1 % From Last joint to base
    
    % displacement from origin i-1 to i in base. Hint: use list(i).doi to help...
%     if( i> 1)
%         d = list(i).doi-list(i-1).doi;
%     else
%         d = list(i).doi;
%     end
    
    n = n - cross(list(i).rii, f);
    
    % Update Force on joint i in base frame
    f = list(i).Fi + f;
    
    % Update Torque on joint i in base frame
    n = n + list(i).Ni + cross(list(i).ri1_i,f);
    
    if linkList(i).isRotary == 1 % Rotational Joint
        % joint i torque is the Z component
        jointTorques(i-num_static) = list(i).Zlast' * n;
   
    elseif linkList(i).isRotary == 0 % Prysmatic
        % joint i force is the Z component
        jointTorques(i-num_static) = list(i).Zlast' * f;
    else
        num_static = num_static-1;
    end
    
end % End Backword Iterations
[Jv,JvDot] = velocityJacobian(linkList,paramList,paramListDot);
end % end newtonEular Function definition