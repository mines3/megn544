function [trans1,trans2,tau] = pickTau(seg1,seg2,transPercent)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
lengthAB= seg1(1,2)-seg1(1,1);
lengthBC= seg2(1,2)-seg2(1,1);

if lengthAB <= lengthBC
    tau=transPercent*lengthAB;
    trans1 = seg1(1,2)-tau;
    trans2 = seg1(1,2)+tau;
else
    tau=transPercent*lengthBC;
    trans1=seg1(1,2)-tau;
    trans2=seg1(1,2)+tau;
end
end

