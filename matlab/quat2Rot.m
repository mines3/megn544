%QUAT2ROT 
%   Input:
%       Q - Quaternion Matrix of 4x1
%   Output: 
%       R - 3x3 Rotation Matrix
%   Dependent Methods: cpMap
%   Description:
%       Function that receives an quaternion and returns a 3x3 rotation
%       rotation matrix.
%
% Jonny Wong
% CWID: 10884664
% MEGN544
% Sept 22, 2021
%
function [R] = quat2Rot(Q)
Q = reshape(Q, 4, 1);
q_0=Q(1);
q_vec=Q(2:end);
q_x=cpMap(q_vec);
R=(q_0^2-q_vec'*q_vec)*eye(3)+2*q_0*q_x+2*q_vec*q_vec';
end