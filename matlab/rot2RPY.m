%ROT2RPY 
%   Input: 
%       rotationM - 3x3 Rotation matrix of an XYZ rotation
%   Output: 
%       Roll - Roll Angles in 2x1 Matrix
%       Pitch - Pitch Angles in 2x1 Matrix
%       Yaw - Yaw Angles in 2x1 Matrix
%   Dependent Methods: None
%   Description:
%       Function that receives a rotation matrix and converts the 
%       rotation matrix(XYZ) to the corresponding 2x1 Roll, Pitch and
%       yaw angles. Output format being [+value; -value]
%
% Jonny Wong
% CWID: 10884664
% MEGN544
% Sept 22, 2021
%
function [roll, pitch, yaw] = rot2RPY(rotationM)

% Calculate pitch
cp = sqrt(rotationM(3,2)^2 + rotationM(3,3)^2);
sp = -rotationM(3,1);

pitch_pos = atan2(sp, cp);
pitch_neg = atan2(sp, -cp);

cy_pos = rotationM(1,1)/cp;
cy_neg = rotationM(1,1)/-cp;

sy_pos = (rotationM(2,1))/cp;
sy_neg = (rotationM(2,1))/-cp;

yaw_pos = atan2(sy_pos, cy_pos);
yaw_neg = atan2(sy_neg, cy_neg);

cr_pos = rotationM(3,3)/cp;
cr_neg = rotationM(3,3)/-cp;

sr_pos = (rotationM(3,2))/cp;
sr_neg = (rotationM(3,2))/-cp;

roll_pos = atan2(sr_pos, cr_pos);
roll_neg = atan2(sr_neg, cr_neg);

if pitch_pos == pi/2
    roll = [atan2(rotationM(1,2), rotationM(2,2)); ...
        atan2(rotationM(1,2), rotationM(2,2))];
    pitch = [pi/2; pi/2];
    yaw = [0;0];
elseif pitch_pos == -pi/2
    roll = [-atan2(rotationM(1,2), rotationM(2,2)); ...
        -atan2(rotationM(1,2), rotationM(2,2))];
    pitch = [-pi/2; -pi/2];
    yaw = [0;0];
else
    roll = [roll_pos; roll_neg];
    pitch = [pitch_pos; pitch_neg];
    yaw = [yaw_pos; yaw_neg];
end

end

