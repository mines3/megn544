%TWIST2TRANSFORM 
%   Input: 
%       twist - 6x1 Twist Matrix
%   Output: 
%       H - 4x4 Homogeneous Transform
%   Dependent Methods: cpMap
%   Description:
%       Function that receives a 6x1 twist matrix and returns a 
%       4x4 homogeneous transformation matrix
%
% Jonny Wong
% CWID: 10884664
% MEGN544
% Sept 22, 2021
%
function [H] = twist2Transform(twist)

v = twist(1:3,1);
omega = twist(4:6,1);

e = expm(cpMap(omega));
theta = norm(omega);
k_hat = omega/theta;
k_hat_cross = cpMap(k_hat);

H = zeros(4,4);

H(1:3,1:3) = e;

H(1:3,4) = (((eye(3)-e)*k_hat_cross) + theta*(k_hat*k_hat')) * v;
H(4,1:3) = zeros(1,3);
H(4,4) = 1;

end
