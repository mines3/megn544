%TRANSFORM2TWIST
%   Input: 
%       H - Homogeneous Transformation Matrix (4x4)
%   Output: 
%       t - 6x1 twist Matrix
%   Dependent Methods: cpMap, rot2AngleAxis
%   Description:
%       Function that receives a homogeneous 4x4 transformation matrix 
%       and returns a 6x1 twist matrix
%
% Jonny Wong
% CWID: 10884664
% MEGN544
% Sept 22, 2021
%
function [t] = transform2Twist(H)

d = H(1:3,4);
rot = H(1:3,1:3);
t = zeros(6,1);

[k_hat, theta] = rot2AngleAxis(rot);

omega = k_hat*theta;

k_hat_cross = cpMap(k_hat);

if theta == 0
    v = d;
else
    v = (((sin(theta)/(2*(1-cos(theta))))*eye(3)) + (((2*(1-cos(theta)))-(theta*sin(theta)))/(2*theta*(1-cos(theta))))*(k_hat*k_hat') - 0.5*k_hat_cross )*d;
end

t(1:3,1) = v;

t(4:6,1) = omega;

end

