%ROT2ANGLEAXIS 
%   Input: 
%       rotationM - 3x3 Rotation Matrix
%   Output: 
%       k - 3x1 Matrix 
%       theta - Scalar value in radians
%   Dependent Methods: None
%   Description:
%       Function to convert a rotation matrix to an angle and axis output
%
% Jonny Wong
% CWID: 10884664
% MEGN544
% Sept 22, 2021
%
function [k, theta, omega] = rot2AngleAxis(R)
    s_theta=0.5*norm([R(3,2)-R(2,3); R(1,3)-R(3,1); R(2,1)-R(1,2)]);
    c_theta=(trace(R)-1)/2;
    theta=atan2(s_theta, c_theta);
    

    if mod(theta, pi)==0
        if theta<0

            term=sqrt((R(1,1)+1)/2);
            k=-[term; (R(1,2)+R(2,1))/(4*term); (R(1,3)+R(3,1))/(4*term)];
        else
            term=sqrt((R(1,1)+1)/2);
            k=[term; (R(1,2)+R(2,1))/(4*term); (R(1,3)+R(3,1))/(4*term)];  
        end
    else
        k=(1/(2*s_theta))*[R(3,2)-R(2,3); R(1,3)-R(3,1); R(2,1)-R(1,2)];
    end

    if theta <= 1e-4
        omega = (1/2)*([R(3,2)-R(2,3); R(1,3)-R(3,1); R(2,1)-R(1,2)]);
    else
        omega = k*theta;
    end

end
